package com.code.jewelry_shop.controller;

import com.code.jewelry_shop.dto.request.forCreate.CRequestParameterDetail;
import com.code.jewelry_shop.dto.request.forUpdate.URequestParameterDetail;
import com.code.jewelry_shop.service.IParameterDetailService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "ParameterDetail", description = "ParameterDetail APIs")
@RestController
@RequestMapping("/api/detail")
public class ParameterDetailController {

    @Autowired
    private IParameterDetailService iParameterDetailService;

    @PostMapping(value = "/{productId}")
    public ResponseEntity<CRequestParameterDetail> saveSpecificationDetails(@PathVariable Long productId,
                                                                                  @RequestBody CRequestParameterDetail cRequestParameterDetail)
    {
        CRequestParameterDetail savedSpecificationDetails = iParameterDetailService.create(productId, cRequestParameterDetail);
        return ResponseEntity.ok(savedSpecificationDetails);
    }

//    @GetMapping(value = "/{productId}")
//    public ResponseEntity<List<Map<String, Object>>> getListDetailByProductId(@PathVariable Long productId) {
//        List<Map<String, Object>> specificationDetails = iParameterDetailService.getListDetailById(productId);
//        return ResponseEntity.ok(specificationDetails);
//    }

    @PutMapping(value = "/{productId}")
    public ResponseEntity<String> updateSpecificationDetailsByProductId(@PathVariable Long productId, @RequestBody URequestParameterDetail uRequestParameterDetail) {
        boolean updateSuccessful = iParameterDetailService.updateParameterDetailByProductId(productId, uRequestParameterDetail);

        if (updateSuccessful) {
            return ResponseEntity.ok("Parameter details updated successfully");
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to update Parameter details");
        }
    }



}
