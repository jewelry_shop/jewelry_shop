package com.code.jewelry_shop.controller;

import com.code.jewelry_shop.entity.BlogType;
import com.code.jewelry_shop.service.IBlogTypeService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "BlogType", description = "BlogType APIs")
@RestController
@RequestMapping("/api/blog-type")
public class BlogTypeController {

    @Autowired
    private IBlogTypeService blogTypeService;

    @PostMapping("/create")
    public ResponseEntity<BlogType> createBlogType(@RequestBody BlogType blogType) {
        BlogType createdBlogType = blogTypeService.createBlogType(blogType);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdBlogType);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteBlogType(@PathVariable Long id) {
        boolean deleted = blogTypeService.deleteBlogType(id);
        if (deleted) {
            return ResponseEntity.ok("BlogType deleted successfully");
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<BlogType> getBlogTypeById(@PathVariable Long id) {
        BlogType blogType = blogTypeService.getBlogType(id);
        if (blogType != null) {
            return ResponseEntity.ok(blogType);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<BlogType>> getAllBlogTypes() {
        List<BlogType> blogTypes = blogTypeService.getAll();
        if (!blogTypes.isEmpty()) {
            return ResponseEntity.ok(blogTypes);
        } else {
            return ResponseEntity.noContent().build();
        }
    }
}

