package com.code.jewelry_shop.controller;


import com.code.jewelry_shop.dto.response.ResponseOrderDetail;
import com.code.jewelry_shop.service.IOrderDetailService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "OrderDetail", description = "OrderDetail APIs")
@RestController
@RequestMapping("/api/order_detail")
public class OrderDetailController {

    @Autowired
    IOrderDetailService orderDetailService;

    @GetMapping(value = "/{orderId}")
    public ResponseEntity<Object> findByOrdersId(@PathVariable(value = "orderId") Long orderId) {
        ResponseOrderDetail orderDetails = orderDetailService.getByOrdersId(orderId);
        return  ResponseEntity.ok().body(orderDetails);
    }
    @GetMapping(value = "/all")
    public ResponseEntity<List<ResponseOrderDetail>> getAllOrderDetail() {
        List<ResponseOrderDetail> orderDetailDTOs = orderDetailService.findAll();
        return ResponseEntity.ok().body(orderDetailDTOs);
    }


}
