package com.code.jewelry_shop.controller;

import com.code.jewelry_shop.dto.request.forCreate.CRequestBlog;
import com.code.jewelry_shop.dto.request.forCreate.URequestBlog;
import com.code.jewelry_shop.dto.response.forBlog.DResponseBlog;
import com.code.jewelry_shop.dto.response.forBlog.LResponseBlog;
import com.code.jewelry_shop.dto.response.forBlog.LResponseBlogLink;
import com.code.jewelry_shop.entity.Blog;
import com.code.jewelry_shop.service.IBlogService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Tag(name = "Blog", description = "Blog APIs")
@RestController
@RequestMapping("/api/blog")
public class BlogController {

    @Autowired
    private IBlogService iBlogService;

    @PostMapping("/create")
    public ResponseEntity<DResponseBlog> createBlog(@ModelAttribute CRequestBlog cRequestBlog) {
        DResponseBlog dResponseBlog = iBlogService.createBlog(cRequestBlog);
        return ResponseEntity.status(HttpStatus.CREATED).body(dResponseBlog);
    }

    @PostMapping("/update")
    public ResponseEntity<DResponseBlog> updateBlog(@ModelAttribute URequestBlog uRequestBlog) {
        DResponseBlog dResponseBlog = iBlogService.updateBlog(uRequestBlog);
        return ResponseEntity.ok(dResponseBlog);
    }

    @PostMapping("/delete/{blogId}")
    public ResponseEntity<String> deleteBlog(@PathVariable Long blogId) {
        boolean deleted = iBlogService.deleteBlog(blogId);
        if (deleted) {
            return ResponseEntity.ok("Blog deleted successfully");
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{blogId}")
    public ResponseEntity<DResponseBlog> getBlogById(@PathVariable Long blogId) {
        DResponseBlog dResponseBlog = iBlogService.getById(blogId);
        if (dResponseBlog != null) {
            return ResponseEntity.ok(dResponseBlog);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<Blog>> getAllBlogs() {
        List<Blog> blogs = iBlogService.getAllBlogs();
        if (!blogs.isEmpty()) {
            return ResponseEntity.ok(blogs);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @GetMapping("/by-link/{linkImage}")
    public ResponseEntity<List<LResponseBlogLink>> getBlogByLink(@PathVariable String linkImage) {
        try {
            List<LResponseBlogLink> blogLinks = iBlogService.getBlogByLink(linkImage);
            if (!blogLinks.isEmpty()) {
                return ResponseEntity.ok(blogLinks);
            } else {
                return ResponseEntity.noContent().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/by-title/{title}")
    public ResponseEntity<List<LResponseBlog>> getBlogByTitle(@PathVariable String title) {
        List<LResponseBlog> blogs = iBlogService.getBlogByTitle(title);
        if (!blogs.isEmpty()) {
            return ResponseEntity.ok(blogs);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @GetMapping("/by-type/{link}")
    public ResponseEntity<List<LResponseBlog>> getBlogByType(@PathVariable String link) {
        List<LResponseBlog> blogs = iBlogService.getAllBlogByType(link);
        if (!blogs.isEmpty()) {
            return ResponseEntity.ok(blogs);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @GetMapping("/review-elements")
    public ResponseEntity<Map<String, List<String>>> reviewElementsNews() {
        Map<String, List<String>> result = iBlogService.reviewElementsBlog();
        if (result != null) {
            return ResponseEntity.ok(result);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
