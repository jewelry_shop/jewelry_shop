package com.code.jewelry_shop.controller;

import com.code.jewelry_shop.dto.response.forList.LResponseProduct;
import com.code.jewelry_shop.entity.Manufacturer;
import com.code.jewelry_shop.service.IManufacturerService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "Manufacturer", description = "Manufacturer APIs")
@RestController
@RequestMapping("/api/manufacturer")
public class ManufacturerController {

    @Autowired
    private IManufacturerService manufacturerService;

    @GetMapping(value = "/all")
    public ResponseEntity<List<Manufacturer>> getAllManufacturer ()
    {
        List<Manufacturer> manufacturers = manufacturerService.getAllManufacturer();
        return ResponseEntity.ok().body(manufacturers);
    }

    @PostMapping(value = "/create")
    public ResponseEntity<Manufacturer> createManufacturer( @RequestBody Manufacturer manufacturer){
        Manufacturer createdManufacturer = manufacturerService.createManufacturer(manufacturer);
        return ResponseEntity.ok().body(createdManufacturer);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<Manufacturer> updateManufacturer( @RequestBody Manufacturer manufacturer){
        Manufacturer updateManufacturer = manufacturerService.updateManufacturer(manufacturer);
        return ResponseEntity.ok().body(updateManufacturer);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteManufacturer(@PathVariable Long id){
        boolean deleteManufacturer = manufacturerService.deleteManufacturer(id);
        if(deleteManufacturer){
            return ResponseEntity.ok("Manufacturer cleared successfully.");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to clear cart.");
    }

    @GetMapping(value = "/{manufacturerId}")
    public ResponseEntity<List<LResponseProduct>> getProductsByManufacturer(@PathVariable Long manufacturerId) {
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setId(manufacturerId);
        List<LResponseProduct> products = manufacturerService.getProductsByManufacturer(manufacturer);
        return ResponseEntity.ok(products);
    }

}
