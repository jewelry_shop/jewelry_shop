package com.code.jewelry_shop.controller;

import com.code.jewelry_shop.dto.request.forCreate.CRequestDiscountOrder;
import com.code.jewelry_shop.dto.request.forUpdate.URequestDiscountOrder;
import com.code.jewelry_shop.dto.response.ResponseDiscountOrder;
import com.code.jewelry_shop.entity.DiscountOrder;
import com.code.jewelry_shop.service.IDiscountOrderService;
import com.code.jewelry_shop.utils.constant.DiscountType;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@Tag(name = "DiscountOrder", description = "DiscountOrder APIs")
@RestController
@RequestMapping("/api/discount-orders")
public class DiscountOrderController {

    @Autowired
    private IDiscountOrderService discountOrderService;

    @GetMapping(value = "/{discountOrderId}")
    public ResponseEntity<DiscountOrder> getDiscountOrderById(@PathVariable Long discountOrderId) {
        DiscountOrder discountOrder = discountOrderService.getDiscountOrderById(discountOrderId);
        return ResponseEntity.ok(discountOrder);
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<DiscountOrder>> getAllDiscountOrders() {
        List<DiscountOrder> discountOrders = discountOrderService.getAllDiscountOrders();
        return ResponseEntity.ok(discountOrders);
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<DiscountOrder>> getAllDiscountOrdersByDiscountType(@RequestParam DiscountType discountType) {
        List<DiscountOrder> discountOrders = discountOrderService.getAllDiscountOrdersByDiscountType(discountType);
        return ResponseEntity.ok(discountOrders);
    }

    @PostMapping(value = "/create")
    public ResponseEntity<DiscountOrder> createDiscountOrder(@RequestBody CRequestDiscountOrder request) {
        if (request.getDiscountType() == null) {
            return ResponseEntity.badRequest().build();
        }
        DiscountOrder discountOrder = discountOrderService.createDiscountOrder(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(discountOrder);
    }

    @PutMapping(value = "/{discountOrderId}")
    public ResponseEntity<DiscountOrder> updateDiscountOrder(@PathVariable Long discountOrderId, @RequestBody URequestDiscountOrder request) {
        DiscountOrder updatedDiscountOrder = discountOrderService.updateDiscountOrder(discountOrderId, request);
        return ResponseEntity.ok(updatedDiscountOrder);
    }

    @DeleteMapping(value = "/{discountOrderId}")
    public ResponseEntity<String> deleteDiscountOrder(@PathVariable Long discountOrderId) {
        boolean isdeleted = discountOrderService.deleteDiscountOrder(discountOrderId);
        if(isdeleted){
            return ResponseEntity.ok().body("DiscountOrder with ID " + discountOrderId + " has been successfully deleted.");
        }else {
        return ResponseEntity.notFound().build();
        }
    }


    @GetMapping(value = "/orders/{orderId}")
    public ResponseEntity<List<ResponseDiscountOrder>> getDiscountOrdersByOrder(@PathVariable Long orderId) {
        List<ResponseDiscountOrder> responseDiscountOrders = discountOrderService.getDiscountOrderByOrders(Collections.singletonList(orderId));
        return ResponseEntity.ok(responseDiscountOrders);
    }
}

