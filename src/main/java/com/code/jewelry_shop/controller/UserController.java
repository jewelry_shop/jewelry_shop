package com.code.jewelry_shop.controller;

import com.code.jewelry_shop.dto.request.forUpdate.URequestUser;
import com.code.jewelry_shop.dto.response.forDetail.DResponseUser;
import com.code.jewelry_shop.dto.response.forList.LResponseUser;
import com.code.jewelry_shop.entity.User;
import com.code.jewelry_shop.service.IUserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Tag(name = "User", description = "User APIs")
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    public IUserService userService;

    @GetMapping(value = "/phone")
    public ResponseEntity<DResponseUser> getUserByPhone(@RequestParam("phone") String phone){
        DResponseUser user = userService.getUserByPhone(phone);
        return ResponseEntity.ok().body(user);
    }

    @PutMapping(value = "/{id}}")
    public ResponseEntity<User> update(@PathVariable("id") Long id, @RequestParam(value = "avatar", required = false) MultipartFile multipartFile, @RequestBody @Valid URequestUser updateUser) throws IOException {
        User result = userService.update(multipartFile, id, updateUser);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping(value = "/users")
    public ResponseEntity<List<LResponseUser>> getAllUsers(){
        List<LResponseUser> users = userService.getAll();
        return ResponseEntity.ok().body(users);
    }

    @GetMapping(value = "/")
    public ResponseEntity<DResponseUser> getUser(@RequestParam("id") Long id) {
        DResponseUser user = userService.getResponseUserByUserId(id);
        return ResponseEntity.ok().body(user);
    }

    @GetMapping(value = "/account")
    public ResponseEntity<User> getUserByAccountId(@RequestParam("accountId") Long id){
        User user = userService.getUserByAccountId(id);
        return ResponseEntity.ok().body(user);
    }
}
