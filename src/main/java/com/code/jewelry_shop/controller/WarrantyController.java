package com.code.jewelry_shop.controller;

import com.code.jewelry_shop.dto.request.forCreate.CRequestWarranty;
import com.code.jewelry_shop.entity.Warranty;
import com.code.jewelry_shop.service.IWarrantyService;
import com.code.jewelry_shop.utils.exception.ApiException;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Warranty", description = "Warranty APIs")
@RestController
@RequestMapping("/warranty")
public class WarrantyController {

    @Autowired
    private IWarrantyService warrantyService;

    @PostMapping("/create")
    public ResponseEntity<Warranty> createWarranty(@RequestBody CRequestWarranty cRequestWarranty) {
        Warranty warranty = warrantyService.createWarranty(cRequestWarranty);
        return ResponseEntity.status(HttpStatus.CREATED).body(warranty);
    }

    @GetMapping("/{serialNumber}")
    public ResponseEntity<Warranty> getWarrantyBySerialNumber(@PathVariable String serialNumber) {
        Warranty warranty = warrantyService.findBySerialNumber(serialNumber);
        if (warranty == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(warranty);
    }

    @PostMapping("/claim/{serialNumber}")
    public ResponseEntity<String> claimWarranty(@PathVariable String serialNumber) {
        try {
            warrantyService.claimWarranty(serialNumber);
            return ResponseEntity.ok("Warranty claimed successfully.");
        } catch (ApiException e) {
            return new ResponseEntity<>(e.getMessage(), e.getHttpStatus());
        }
    }
}
