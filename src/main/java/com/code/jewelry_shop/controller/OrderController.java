package com.code.jewelry_shop.controller;

import com.code.jewelry_shop.dto.request.forUpdate.URequestOrder;
import com.code.jewelry_shop.dto.response.ResponseOrder;
import com.code.jewelry_shop.dto.response.forList.LResponseUser_Oder;
import com.code.jewelry_shop.entity.Order;
import com.code.jewelry_shop.service.IOrderService;
import com.code.jewelry_shop.utils.exception.ApiException;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "Order", description = "Order APIs")
@RestController
@RequestMapping("/api/order")
public class OrderController {

    @Autowired
    private IOrderService orderService;

    @GetMapping(value = "/all")
    public ResponseEntity<List<LResponseUser_Oder>> getAllOrders() {
        List<LResponseUser_Oder> orders = orderService.getAll();
        return ResponseEntity.ok().body(orders);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Order> getOrdersById(@PathVariable Long id) {
        Order orders = orderService.getById(id);
        return ResponseEntity.ok().body(orders);
    }

    @GetMapping(value = "/get_email/{email}")
    public ResponseEntity <List<ResponseOrder>> getAllByEmail(@PathVariable String email) {
        List<ResponseOrder> orders = orderService.getAllByEmailContains(email);
        return ResponseEntity.ok().body(orders);
    }


    @PutMapping(value = "/update")
    public ResponseEntity<Order> updateOrder(@Valid @RequestBody URequestOrder uRequestOrder) {
        Order result = orderService.update(uRequestOrder);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/{id}/apply-discount")
    public ResponseEntity<?> applyDiscountToOrder(@PathVariable("id") Long orderId, @RequestParam("discountCode") String discountCode) {
        try {
            Order order = orderService.getById(orderId);
            order = orderService.applyDiscount(order, discountCode);
            return ResponseEntity.ok(order);
        } catch (ApiException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping("/{id}/confirm-payment")
    public ResponseEntity<?> confirmPayment(@PathVariable("id") Long orderId, @RequestParam("paymentName") String paymentName, @RequestParam("success") boolean success) {
        try {
            boolean confirmed = orderService.confirmPayment(orderId, paymentName, success);
            if (confirmed) {
                return ResponseEntity.ok("Payment confirmed successfully.");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to confirm payment.");
            }
        } catch (ApiException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to confirm payment.");
        }
    }
}

