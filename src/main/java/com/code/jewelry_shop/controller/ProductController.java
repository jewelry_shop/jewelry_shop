package com.code.jewelry_shop.controller;

import com.code.jewelry_shop.dto.request.forCreate.CRequestProduct;
import com.code.jewelry_shop.dto.request.forUpdate.URequestProduct;
import com.code.jewelry_shop.dto.response.forDetail.DResponseProduct;
import com.code.jewelry_shop.dto.response.forList.LResponseProduct;
import com.code.jewelry_shop.entity.CategoryC;
import com.code.jewelry_shop.entity.Manufacturer;
import com.code.jewelry_shop.entity.Product;
import com.code.jewelry_shop.service.IProductColorService;
import com.code.jewelry_shop.service.IProductService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Tag(name = "Product", description = "Product APIs")
@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    private IProductColorService productColorService;

    @Autowired
    private IProductService productService;

    @PostMapping("/add")
    public ResponseEntity<DResponseProduct> addProduct(@ModelAttribute("productDTO") CRequestProduct cRequestProduct) {
        DResponseProduct responseDTO = productService.addProduct(cRequestProduct);
        Long productId = responseDTO.getProduct().getId();
        List<Long> colors = cRequestProduct.getColors();
        Map<String, Object> createdProductColor = productColorService.createProductColor(productId, colors);
        List<Map<String, Object>> productColors = (List<Map<String, Object>>) createdProductColor.get("productColors");
        responseDTO.setProductColors(productColors);
        return ResponseEntity.ok().body(responseDTO);
    }


    @GetMapping("/all")
    public ResponseEntity<List<Product>> getAllProducts() {
        List<Product> products = productService.getAllProducts();
        return ResponseEntity.ok(products);
    }

    @GetMapping("/{productId}")
    public ResponseEntity<DResponseProduct> getProductById(@PathVariable Long productId) {
        DResponseProduct productDTO = productService.getProductById(productId);

        if (productDTO == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(productDTO);
    }

    @DeleteMapping("delete/{productId}")
    public ResponseEntity<String> deleteProduct(@PathVariable Long productId) {
        boolean deleted = productService.deleteProduct(productId);
        if (deleted) {
            return ResponseEntity.ok("Product deleted successfully");
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/update")
    public ResponseEntity<URequestProduct> updateProduct(@ModelAttribute URequestProduct updatedProductDto) {
        URequestProduct updatedProduct = productService.updateProductInfo(updatedProductDto);
        return ResponseEntity.ok(updatedProduct);
    }

    @GetMapping("/search")
    public ResponseEntity<List<LResponseProduct>> searchProductsByName(@RequestParam("keyword") String keyword) {
        List<LResponseProduct> products = productService.searchProducts(keyword);
        return ResponseEntity.ok(products);
    }

    @GetMapping(value = "/page")
    public ResponseEntity<Page<LResponseProduct>> getAllProductDTOs(@RequestParam(defaultValue = "0" ) int page, @RequestParam(defaultValue = "ASC") Sort.Direction direction) {
        Page<LResponseProduct> productDTOs = productService.getAllProductDTOs(page, direction);
        return ResponseEntity.ok(productDTOs);
    }

    @GetMapping(value = "/SortingASC")
    public ResponseEntity<Page<LResponseProduct>> SortingAsc(@RequestParam(defaultValue = "0" ) int page, @RequestParam(defaultValue = "ASC") Sort.Direction direction) {
        Page<LResponseProduct> productDTOs = productService.Sorting(page, direction);
        return ResponseEntity.ok(productDTOs);
    }

    @GetMapping(value = "/SortingDESC")
    public ResponseEntity<Page<LResponseProduct>> SortingDesc(@RequestParam(defaultValue = "0" ) int page, @RequestParam(defaultValue = "DESC") Sort.Direction direction) {
        Page<LResponseProduct> productDTOs = productService.Sorting(page, direction);
        return ResponseEntity.ok(productDTOs);
    }

    @GetMapping(value = "/admin/page")
    public ResponseEntity<Page<Product>> getAllProducts(@RequestParam(defaultValue = "0") int page) {
        Page<Product> products = productService.getAllProducts(page);
        return ResponseEntity.ok(products);
    }

    @GetMapping("/discount/category/{categoryId}")
    public ResponseEntity<List<LResponseProduct>> getAllProductDiscountByCategory(@PathVariable Long categoryId) {
        CategoryC categoryC = new CategoryC();
        categoryC.setId(categoryId);
        List<LResponseProduct> products = productService.getAllProductDiscountByCategory(categoryC);
        return ResponseEntity.ok(products);
    }

    @GetMapping("/discount/manufacturer/{manufacturerId}")
    public ResponseEntity<List<LResponseProduct>> getAllProductDiscountByManufacture(@PathVariable Long manufacturerId) {
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setId(manufacturerId);
        List<LResponseProduct> products = productService.getAllProductDiscountByManufacture(manufacturer);
        return ResponseEntity.ok(products);
    }

    @GetMapping("/discount/percentage/{discountPercentage}")
    public ResponseEntity<List<LResponseProduct>> getAllProductDiscountByDiscountPercentage(@PathVariable BigDecimal discountPercentage) {
        List<LResponseProduct> products = productService.getAllProductDiscountByDiscountPercentage(discountPercentage);
        return ResponseEntity.ok(products);
    }



}