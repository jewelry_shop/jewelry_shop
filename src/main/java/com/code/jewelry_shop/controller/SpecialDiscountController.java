package com.code.jewelry_shop.controller;

import com.code.jewelry_shop.dto.request.forCreate.CRequestSpecialDiscount;
import com.code.jewelry_shop.entity.SpecialDiscount;
import com.code.jewelry_shop.service.ISpecialDiscountService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "SpecialDiscount", description = "SpecialDiscount APIs")
@RestController
@RequestMapping("/api/special-discount")
public class SpecialDiscountController {

    @Autowired
    ISpecialDiscountService iSpecialDiscountService;


    @PostMapping("/add")
    public ResponseEntity<SpecialDiscount> addSpecialDiscount(@RequestBody CRequestSpecialDiscount cRequestSpecialDiscount) {
        SpecialDiscount specialDiscount = iSpecialDiscountService.insert(cRequestSpecialDiscount);
        return ResponseEntity.status(HttpStatus.CREATED).body(specialDiscount);
    }

    @GetMapping("/get")
    public ResponseEntity<SpecialDiscount> getSpecialDiscount(@RequestParam("id") Long id){
        SpecialDiscount specialDiscount = iSpecialDiscountService.getOne(id);
        return ResponseEntity.ok(specialDiscount);
    }

    @GetMapping("/all")
    public ResponseEntity<List<SpecialDiscount>> getAllSpecialDiscount(){
        List<SpecialDiscount> specialDiscountList = iSpecialDiscountService.getAll();
        return ResponseEntity.ok(specialDiscountList);
    }

    @PostMapping("/update")
    public ResponseEntity<SpecialDiscount> updateSpecialDiscount(@RequestBody SpecialDiscount SpecialDiscount) {
        SpecialDiscount specialDiscount = iSpecialDiscountService.update(SpecialDiscount);
        return ResponseEntity.ok(specialDiscount);
    }

    @GetMapping("")
    public ResponseEntity<List<SpecialDiscount>> getSpecialDiscountByName(@RequestParam("keyword") String string){
        List<SpecialDiscount> specialDiscountList = iSpecialDiscountService.search(string);
        return ResponseEntity.ok(specialDiscountList);
    }

}
