package com.code.jewelry_shop.controller;

import com.code.jewelry_shop.dto.request.authen.ChangePassword;
import com.code.jewelry_shop.dto.response.authen.EmailResult;
import com.code.jewelry_shop.dto.request.authen.Register;
import com.code.jewelry_shop.dto.response.ResponseAccount;
import com.code.jewelry_shop.dto.response.forDetail.DResponseAccount;
import com.code.jewelry_shop.security.jwt.JwtAuthResponse;
import com.code.jewelry_shop.service.IAccountService;
import com.code.jewelry_shop.utils.constant.AuthoritiesConstants;
import com.code.jewelry_shop.utils.exception.ApiException;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Tag(name = "Account", description = "Account APIs")
@RestController
@RequestMapping("/api/account")
public class AccountController {

    @Autowired
    private IAccountService accountService;

    @PostMapping(value = "/register")
    public ResponseEntity<DResponseAccount> registerAccount(@Valid @RequestBody Register register){
        DResponseAccount newAccount = accountService.registerAccount(register);
        return ResponseEntity.status(HttpStatus.CREATED).body(newAccount);
    }

    @PostMapping(value = "/login")
    public ResponseEntity<Object> loginAccount(@Valid @RequestParam("email") String email, @RequestParam("password") String password) throws AuthenticationException {
        try{
            JwtAuthResponse jwtAuthResponse = accountService.checkLogin(email, password);
            String jwt = jwtAuthResponse.getToken();
            ResponseAccount accountResponse = jwtAuthResponse.getAccount();
            return ResponseEntity.ok(new JwtAuthResponse(jwt, accountResponse));
        }catch (AuthenticationException ae){
            return new ResponseEntity<>(Collections.singletonMap("AuthenticationException", ae.getLocalizedMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(value = "/{username}")
    public ResponseEntity<ResponseAccount> getAccountByUsername(@PathVariable String username){
        return accountService.getByUsername(username)
                .map(account -> ResponseEntity.ok().body(account))
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/")
    public ResponseEntity<List<ResponseAccount>> getAllAccounts(){
        List<ResponseAccount> accounts = accountService.getAllAccount();
        return ResponseEntity.ok().body(accounts);
    }

    @PutMapping(value = "/changeRole")
    public ResponseEntity<String> changeAccountRole(@RequestParam("id") Long id){
        accountService.changeRole(id);
        return ResponseEntity.ok().body("{\"message\": \"Update role successfully!\"}");
    }

    @PutMapping(value = "/{id}/status")
    public ResponseEntity<String> changeAccountStatus(@PathVariable("id") Long accountId) {
        boolean statusChanged = accountService.changeStatus(accountId);
        if (statusChanged) {
            return ResponseEntity.ok("Account status changed successfully.");
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteAccount(@PathVariable Long id) {
        boolean deleted = accountService.deleteAccount(id);
        if (deleted) {
            return ResponseEntity.ok().body("Account with ID " + id + " has been successfully deleted.");
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to delete account.");
        }
    }

    @GetMapping(value = "/role")
    public ResponseEntity<List<ResponseAccount>> getAccountByRole(@RequestParam("role") String role){
        AuthoritiesConstants roleEnum = AuthoritiesConstants.valueOf(role);
        List<ResponseAccount> accountsByRole = accountService.getByRole(roleEnum);
        return ResponseEntity.ok(accountsByRole);
    }

//    @GetMapping(value = "/search")
//    public ResponseEntity<List<Account>> search(@RequestParam("keyword") String keyword){
//        List<Account> searchList = accountRepository.findByUsernameContaining(keyword);
//        return ResponseEntity.ok(searchList);
//    }

    @PutMapping(value = "/changePassword")
    public ResponseEntity<String> changePassword(@RequestBody ChangePassword changePassword){
        accountService.changePassword(changePassword);
        return ResponseEntity.ok().body("{\"message\": \"Change password successfully!\"}");
    }

    @PostMapping(value = "/reset")
    public ResponseEntity<EmailResult> sendResetPassword(@RequestBody ChangePassword changePassword) {
        try {
            EmailResult result = accountService.sendResetPassword(changePassword);
            return ResponseEntity.ok(result);
        } catch (ApiException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new EmailResult(false, e.getMessage()));
        }
    }

    @PutMapping(value = "/reset/confirm")
    public ResponseEntity<Optional<ResponseAccount>> resetPassword(@Valid @RequestBody ChangePassword changePassword) {
        try {
            Optional<ResponseAccount> account = accountService.resetPassword(changePassword);
            return ResponseEntity.ok(account);
        } catch (ApiException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}
