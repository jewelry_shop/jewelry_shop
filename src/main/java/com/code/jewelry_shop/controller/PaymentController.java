package com.code.jewelry_shop.controller;

import com.code.jewelry_shop.entity.Payment;
import com.code.jewelry_shop.service.IPaymentService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Tag(name = "Payment", description = "Payment APIs")
@RestController
@RequestMapping("/api/payment")
public class PaymentController {

    @Autowired
    private IPaymentService paymentService;

    @PostMapping("/add")
    public ResponseEntity<Payment> createPayment(@RequestBody Payment payment){
        Payment createdPayment = paymentService.createPayment(payment);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdPayment);
    }
    @GetMapping("/all")
    public ResponseEntity<List<Payment>> getAllPayment(){
        List<Payment> payments = paymentService.getAllPayment();
        return ResponseEntity.ok(payments);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Payment> getPaymentById(@PathVariable("id") Long id){
        Optional<Payment> payment = paymentService.getPaymentById(id);
        return payment.map(value -> new ResponseEntity<>(value, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<Payment> updatePaymentById(@PathVariable("id") Long id, @RequestBody Payment payment){
        Optional<Payment> exitPayment = paymentService.getPaymentById(id);
        if (exitPayment.isPresent()) {
            payment.setId(id);
            Payment updatePayment = paymentService.updatePayment(payment);
            return ResponseEntity.ok(updatePayment);
        }
        return ResponseEntity.notFound().build();
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deletePayment(@PathVariable("id") Long id) {
        boolean deleted = paymentService.deletePaymentByTd(id);
        if (deleted) {
            return ResponseEntity.ok().body("Payment deleted successfully");
        }
        return ResponseEntity.notFound().build();
    }

}
