package com.code.jewelry_shop.controller;

import com.code.jewelry_shop.entity.Color;
import com.code.jewelry_shop.service.IColorService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Tag(name = "Color", description = "color APIs")
@RestController
@RequestMapping("/api/color")
public class ColorController {

    @Autowired
    private IColorService colorService;

    @PostMapping("/color")
    public ResponseEntity<Color> createColor(@RequestBody Color color) {
        Color createdColor = colorService.createColor(color);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdColor);
    }

    @GetMapping("/colors")
    public ResponseEntity<List<Color>> getAllColors() {
        List<Color> colors = colorService.getAllColor();
        return ResponseEntity.ok().body(colors);
    }

    @GetMapping("/color/{colorId}")
    public ResponseEntity<Color> getColorById(@PathVariable Long colorId) {
        Optional<Color> colorOptional = colorService.getColorById(colorId);
        return colorOptional.map(color -> ResponseEntity.ok().body(color))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/color/{colorId}")
    public ResponseEntity<Color> updateColor(@PathVariable Long colorId, @RequestBody Color colorDetails) {
        Optional<Color> colorOptional = colorService.getColorById(colorId);
        if (colorOptional.isPresent()) {
            Color updatedColor = colorService.updateColor(colorDetails);
            return ResponseEntity.ok().body(updatedColor);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/color/{colorId}")
    public ResponseEntity<?> deleteColorById(@PathVariable Long colorId) {
        boolean isDeleted = colorService.deleteColorById(colorId);
        if (isDeleted) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
