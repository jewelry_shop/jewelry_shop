package com.code.jewelry_shop.controller;

import com.code.jewelry_shop.dto.request.forUpdate.URequestProductColor;
import com.code.jewelry_shop.dto.response.ResponseProductColor;
import com.code.jewelry_shop.service.IProductColorService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "ProductColor", description = "ProductColor APIs")
@RestController
@RequestMapping("/api/product_color")
public class ProductColorController {

    @Autowired
    private IProductColorService productColorService;

    @GetMapping(value = "/get-list/{productId}")
    public ResponseEntity<ResponseProductColor> getListDetailById(@PathVariable Long productId) {
        ResponseProductColor productColor = productColorService.getListDetailById(productId);
        return ResponseEntity.ok(productColor);
    }

    @PutMapping(value = "/edit/{productId}")
    public ResponseEntity<ResponseProductColor> updateProductColor(@PathVariable Long productId, @RequestBody URequestProductColor uProductColor) {
        ResponseProductColor productColorDTO = productColorService.updateProductColors(productId, uProductColor.getColors());
        return ResponseEntity.ok(productColorDTO);
    }

}