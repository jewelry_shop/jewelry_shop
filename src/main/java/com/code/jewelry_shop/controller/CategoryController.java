package com.code.jewelry_shop.controller;

import com.code.jewelry_shop.dto.response.forList.LResponseProduct;
import com.code.jewelry_shop.entity.CategoryC;
import com.code.jewelry_shop.service.ICategoryCService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Tag(name = "Category", description = "Category APIs")
@RestController
@RequestMapping("/api/category")
public class CategoryController {
    @Autowired
    private ICategoryCService categoryCService;

    @PostMapping(value = "/create")
    public ResponseEntity<CategoryC> createCategory(@RequestBody CategoryC category) {
        CategoryC createdCategory = categoryCService.createCategoryC(category);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdCategory);
    }

    @GetMapping(value = "/")
    public ResponseEntity<List<CategoryC>> getAllCategories() {
        List<CategoryC> categories = categoryCService.getAllCategoriesC();
        return ResponseEntity.ok(categories);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<CategoryC> updateCategory(@PathVariable("id") Long categoryId, @RequestBody CategoryC category) {
        Optional<CategoryC> existingCategory = categoryCService.getCategoryCById(categoryId);
        if (existingCategory.isPresent()) {
            category.setId(categoryId);
            CategoryC updatedCategory = categoryCService.updateCategoryC(category);
            return ResponseEntity.ok(updatedCategory);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteCategory(@PathVariable("id") Long categoryId) {
        boolean deleted = categoryCService.deleteCategoryCById(categoryId);
        if (deleted) {
            return ResponseEntity.ok("Category deleted successfully");
        }
        return new ResponseEntity<>("Category not found", HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/{categoryId}")
    public ResponseEntity<List<LResponseProduct>> getProductsByCategoryC(@PathVariable Long categoryId) {
        CategoryC categoryC = new CategoryC();
        categoryC.setId(categoryId);
        List<LResponseProduct> products = categoryCService.getProductsByCategoryC(categoryC);
        return ResponseEntity.ok(products);
    }
}
