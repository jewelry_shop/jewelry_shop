package com.code.jewelry_shop.controller;


import com.code.jewelry_shop.dto.response.ResponseCart;
import com.code.jewelry_shop.dto.response.ResponseCartDetail;
import com.code.jewelry_shop.dto.response.forDetail.DCResponseCart;
import com.code.jewelry_shop.entity.Cart;
import com.code.jewelry_shop.entity.User;
import com.code.jewelry_shop.repository.CartRepository;
import com.code.jewelry_shop.service.ICartService;
import com.code.jewelry_shop.service.IUserService;
import com.code.jewelry_shop.utils.exception.ApiException;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "Cart", description = "Cart APIs")
@RestController
@RequestMapping("/cart")
public class CartController {
    
    @Autowired
    private ICartService cartService;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private IUserService userService;

    @GetMapping(value ="/{id}")
    public ResponseEntity<ResponseCart> getCartById(@PathVariable Long id) {
        ResponseCart ResponseCart = cartService.getCartById(id);
        return ResponseEntity.ok(ResponseCart);
    }

    @PostMapping(value = "/users")
    public ResponseEntity<ResponseCart> addProductToCart(
            @RequestParam Long userId,
            @RequestParam Long productId,
            @RequestParam Long colorId
    ) {
        ResponseCart ResponseCart = cartService.addProductToCart(userId, productId, colorId);
        return ResponseEntity.ok(ResponseCart);
    }

    @DeleteMapping(value = "/clear/{cartId}")
    public ResponseEntity<String> clearCart(@PathVariable Long cartId) {
        boolean clear = cartService.clearCart(cartId);
        if(clear){
            return ResponseEntity.ok("Cart cleared successfully.");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to clear cart.");
    }

    @PostMapping(value ="/remove-cart-detail/{cartDetailId}")
    public ResponseEntity<String> removeCartDetail(@PathVariable Long cartDetailId) {
        boolean deleted = cartService.removeCartDetail(cartDetailId);
        if(deleted){
            return ResponseEntity.ok().body("Cart cleared successfully.");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to delete cart detail.");
    }

    @PutMapping(value ="/{cartDetailId}")
    public ResponseEntity<ResponseCartDetail> updateCartDetail(
            @PathVariable Long cartDetailId,
            @RequestBody ResponseCartDetail cartDetail,
            @RequestParam("quantityChange") int quantityChange
    ) {
        try {
            ResponseCartDetail updatedCartDetail = cartService.updateCartDetail(cartDetailId, cartDetail, quantityChange);
            return ResponseEntity.ok(updatedCartDetail);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @GetMapping(value = "/all-cart-details")
    public ResponseEntity<List<ResponseCartDetail>> getAllCartDetails(@RequestParam Long userId) {
        List<ResponseCartDetail> cartDetails = cartService.getAllCartDetails(userId);
        return ResponseEntity.ok(cartDetails);
    }

    @GetMapping(value = "/user")
    public ResponseEntity<ResponseCart> getCartByUserId(@RequestParam Long userId) {
        ResponseCart carts = cartService.getCartByUserId(userId);
        return ResponseEntity.ok(carts);
    }

    @PostMapping("/checkout")
    public ResponseEntity<?> checkoutCart(@RequestParam("userId") Long userId) {

        User user = userService.getUserByUserId(userId);
        if (user == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
        }
        Cart cart = cartRepository.findByUserId(user.getId()).get();

        try {
            DCResponseCart response = cartService.checkoutCart(cart, user);
            return ResponseEntity.ok(response);
        } catch (ApiException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}

