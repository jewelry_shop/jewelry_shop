package com.code.jewelry_shop.service;

import com.code.jewelry_shop.dto.request.forUpdate.URequestDiscountCode;
import com.code.jewelry_shop.entity.DiscountCode;
import com.code.jewelry_shop.dto.request.forCreate.CRequestDiscountCode;
import jakarta.transaction.Transactional;

public interface IDiscountCodeService {

    @Transactional
    DiscountCode create(CRequestDiscountCode cRequestDiscountCode);

    DiscountCode getDiscountCodeByCode(String name);

    @Transactional
    DiscountCode update (URequestDiscountCode uRequestDiscountCode);

    @Transactional
    void delete(Long id);

//    List<LResponseDiscountCode> search(String s);
}
