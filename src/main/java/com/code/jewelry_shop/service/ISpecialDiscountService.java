package com.code.jewelry_shop.service;

import com.code.jewelry_shop.dto.request.forCreate.CRequestSpecialDiscount;
import com.code.jewelry_shop.entity.SpecialDiscount;

import java.util.List;

public interface ISpecialDiscountService {
    SpecialDiscount insert(CRequestSpecialDiscount cRequestDiscountCombo);

    SpecialDiscount getOne(Long id);

    List<SpecialDiscount> getAll();

    SpecialDiscount update(SpecialDiscount discountCombo);

    List<SpecialDiscount> search(String s);


}
