package com.code.jewelry_shop.service;

import com.code.jewelry_shop.dto.response.forList.LResponseProduct;
import com.code.jewelry_shop.entity.CategoryC;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Optional;

public interface ICategoryCService {

    @Transactional
    CategoryC createCategoryC(CategoryC category);

    List<CategoryC> getAllCategoriesC();

    Optional<CategoryC> getCategoryCById(Long categoryId);

    @Transactional
    CategoryC updateCategoryC(CategoryC categoryC);

    @Transactional
    boolean deleteCategoryCById(Long categoryCId);

    List<LResponseProduct> getProductsByCategoryC(CategoryC categoryC);
}
