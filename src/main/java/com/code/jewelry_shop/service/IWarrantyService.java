package com.code.jewelry_shop.service;

import com.code.jewelry_shop.dto.request.forCreate.CRequestWarranty;
import com.code.jewelry_shop.entity.Warranty;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

public interface IWarrantyService {

    Warranty createWarranty(CRequestWarranty cRequestWarranty);

    Warranty findBySerialNumber(String serialNumber);

    void claimWarranty(String serialNumber);
}
