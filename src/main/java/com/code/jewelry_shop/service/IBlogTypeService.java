package com.code.jewelry_shop.service;

import com.code.jewelry_shop.entity.BlogType;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IBlogTypeService {

    @Transactional
    BlogType createBlogType(BlogType blogType);


//    BlogType updateBlogType(BlogType blogType);

    @Transactional
    boolean deleteBlogType(Long id);

    BlogType getBlogType(Long id);

    List<BlogType> getAll();

}
