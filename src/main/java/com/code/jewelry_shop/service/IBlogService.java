package com.code.jewelry_shop.service;


import com.code.jewelry_shop.dto.request.forCreate.CRequestBlog;
import com.code.jewelry_shop.dto.request.forCreate.URequestBlog;
import com.code.jewelry_shop.dto.response.forBlog.DResponseBlog;
import com.code.jewelry_shop.dto.response.forBlog.LResponseBlog;
import com.code.jewelry_shop.dto.response.forBlog.LResponseBlogLink;
import com.code.jewelry_shop.entity.Blog;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface IBlogService {

    @Transactional
    DResponseBlog createBlog(CRequestBlog cRequestBlog);

    @Transactional
    DResponseBlog updateBlog(URequestBlog urequestBlog);

    @Transactional
    boolean deleteBlog(Long id);

    DResponseBlog getById(Long id);

    List<Blog> getAllBlogs();

    List<LResponseBlogLink> getBlogByLink(String linkImage) throws IOException;

    List<LResponseBlog> getBlogByTitle(String title);

    List<LResponseBlog> getAllBlogByType(String link);

    Map<String, List<String>> reviewElementsBlog();

//    ResponseBlogDescription getDescriptionByLink(String linkContent, String link);
//
//    List<ResponseRandomRelatedPost> getRelatedPostWithType(String linkContent, String link);
//
//    List<ResponseRandomMorePost> getMorePostWithType(String linkContent, String link);
//
//    List<ResponseBlogType> getListBlogType();
}
