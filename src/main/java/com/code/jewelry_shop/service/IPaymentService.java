package com.code.jewelry_shop.service;

import com.code.jewelry_shop.entity.Payment;

import java.util.List;
import java.util.Optional;

public interface IPaymentService {
    Payment createPayment(Payment payment);
    List<Payment> getAllPayment();
    Optional<Payment> getPaymentById(Long id);
    Payment updatePayment(Payment payment);
    boolean deletePaymentByTd(Long id);
}
