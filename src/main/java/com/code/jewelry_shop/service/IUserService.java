package com.code.jewelry_shop.service;

import com.code.jewelry_shop.dto.request.authen.Register;
import com.code.jewelry_shop.dto.request.forUpdate.URequestUser;
import com.code.jewelry_shop.dto.response.forDetail.DResponseUser;
import com.code.jewelry_shop.dto.response.forList.LResponseUser;
import com.code.jewelry_shop.entity.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IUserService {

    User create(Register register);

    User update(MultipartFile multipartFile, Long id, URequestUser updateUser);

    List<LResponseUser> getAll();

    DResponseUser getResponseUserByUserId(Long id);

    DResponseUser getResponseUserByAccountId(Long id);

    User getUserByUserId(Long id);

    User getUserByAccountId(Long id);

    DResponseUser getUserByPhone(String phone);

}
