package com.code.jewelry_shop.service;

import com.code.jewelry_shop.dto.request.forCreate.CRequestDiscountOrder;
import com.code.jewelry_shop.dto.request.forCreate.CRequestVoucher;
import com.code.jewelry_shop.dto.request.forUpdate.URequestVoucher;
import com.code.jewelry_shop.entity.Voucher;

public interface IVoucherService {

    Voucher create(CRequestVoucher cRequestVoucher);

    Voucher getVoucherByCode(String code);

    Voucher update(URequestVoucher uRequestVoucher);

    void delete(Long id);

//    List<LResponseVoucher> search(String s);

}
