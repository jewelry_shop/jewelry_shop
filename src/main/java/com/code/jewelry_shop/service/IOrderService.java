package com.code.jewelry_shop.service;

import com.code.jewelry_shop.dto.request.forCreate.CRequestOrder;
import com.code.jewelry_shop.dto.request.forUpdate.URequestOrder;
import com.code.jewelry_shop.dto.response.ResponseOrder;
import com.code.jewelry_shop.dto.response.forList.LResponseOrder;
import com.code.jewelry_shop.dto.response.forList.LResponseUser_Oder;
import com.code.jewelry_shop.entity.Order;
import com.code.jewelry_shop.entity.User;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Optional;

public interface IOrderService {

    @Transactional
    Order create(CRequestOrder cRequestOrder, User loggedInUser);

    List<LResponseUser_Oder> getAll();

    Order getById(Long id);

    List<ResponseOrder> getAllByEmailContains(String email);

    @Transactional
    Order update(URequestOrder uRequestOrder);

    @Transactional
    Order applyDiscount(Order order, String discountCode);

    @Transactional
    boolean confirmPayment(Long orderId, String paymentName, Boolean success);
}
