package com.code.jewelry_shop.service;

import com.code.jewelry_shop.dto.response.ResponseCart;
import com.code.jewelry_shop.dto.response.ResponseCartDetail;
import com.code.jewelry_shop.dto.response.forDetail.DCResponseCart;
import com.code.jewelry_shop.entity.Cart;
import com.code.jewelry_shop.entity.User;
import jakarta.transaction.Transactional;

import java.util.List;

public interface ICartService {

    ResponseCart getCartById(Long userId);

    List<ResponseCartDetail> getCartDetailsByCartId(Long userId);

    @Transactional
    ResponseCart addProductToCart(Long userId, Long productId, Long colorId);

    @Transactional
    boolean clearCart(Long userId);

    @Transactional
    boolean removeCartDetail(Long cartDetailId);

    @Transactional
    ResponseCartDetail updateCartDetail(Long cartDetailId, ResponseCartDetail cartDetail, int quantityChange);

    List<ResponseCartDetail> getAllCartDetails(Long userId);

    DCResponseCart checkoutCart(Cart cart, User user);

    public ResponseCart getCartByUserId(Long userId);
}
