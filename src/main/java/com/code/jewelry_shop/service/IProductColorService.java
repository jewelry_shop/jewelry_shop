package com.code.jewelry_shop.service;

import com.code.jewelry_shop.dto.response.ResponseProductColor;

import java.util.List;
import java.util.Map;

public interface IProductColorService {

    Map<String, Object> createProductColor(Long productId, List<Long> colorIds);

    ResponseProductColor getListDetailById (Long productId);

    ResponseProductColor updateProductColors(Long productId, List<Long> newColors);

}