package com.code.jewelry_shop.service;

import com.code.jewelry_shop.dto.request.forCreate.CRequestParameterDetail;
import com.code.jewelry_shop.dto.request.forUpdate.URequestParameterDetail;


public interface IParameterDetailService {

    CRequestParameterDetail create(Long productId, CRequestParameterDetail cRequestParameterDetail);

    boolean updateParameterDetailByProductId(Long productId, URequestParameterDetail uRequestParameterDetails);

}