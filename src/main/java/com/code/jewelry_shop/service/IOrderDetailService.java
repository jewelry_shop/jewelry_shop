package com.code.jewelry_shop.service;

import com.code.jewelry_shop.dto.response.ResponseOrderDetail;
import com.code.jewelry_shop.entity.Cart;
import com.code.jewelry_shop.entity.Order;
import com.code.jewelry_shop.entity.OrderDetail;
import jakarta.transaction.Transactional;

import java.util.List;

public interface IOrderDetailService {

    @Transactional
    List<ResponseOrderDetail> createOrderDetail(Cart cart, Order order);

    ResponseOrderDetail getByOrdersId(Long orderId);

    List<ResponseOrderDetail> findAll();


    List<OrderDetail> getOrderDetailsByOrderId(Long orderId);

}
