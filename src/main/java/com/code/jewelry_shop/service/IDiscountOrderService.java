package com.code.jewelry_shop.service;

import com.code.jewelry_shop.dto.request.forCreate.CRequestDiscountOrder;
import com.code.jewelry_shop.dto.request.forUpdate.URequestDiscountOrder;
import com.code.jewelry_shop.dto.response.ResponseDiscountOrder;
import com.code.jewelry_shop.entity.DiscountOrder;
import com.code.jewelry_shop.utils.constant.DiscountType;
import jakarta.transaction.Transactional;

import java.util.List;

public interface IDiscountOrderService {


    @Transactional
    DiscountOrder createDiscountOrder(CRequestDiscountOrder cRequestDiscountOrder);

    @Transactional
    DiscountOrder updateDiscountOrder(Long discountOrderId, URequestDiscountOrder uRequestDiscountOrder);

    @Transactional
    boolean deleteDiscountOrder(Long discountOrderId);

    List<DiscountOrder> getAllDiscountOrders();

    List<DiscountOrder> getAllDiscountOrdersByDiscountType(DiscountType discountType);

    DiscountOrder getDiscountOrderById(Long discountOrderId);

    List<ResponseDiscountOrder> getDiscountOrderByOrders(List<Long> ordersId);

    DiscountOrder getDiscountOrderByCode(String code);


}
