package com.code.jewelry_shop.service;

import com.code.jewelry_shop.dto.request.forCreate.CRequestProduct;
import com.code.jewelry_shop.dto.request.forUpdate.URequestProduct;
import com.code.jewelry_shop.dto.response.forDetail.DResponseProduct;
import com.code.jewelry_shop.dto.response.forList.LResponseProduct;
import com.code.jewelry_shop.entity.CategoryC;
import com.code.jewelry_shop.entity.Manufacturer;
import com.code.jewelry_shop.entity.Product;
import com.code.jewelry_shop.entity.SpecialDiscount;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.math.BigDecimal;
import java.util.List;

public interface IProductService {

    @Transactional
    DResponseProduct addProduct(CRequestProduct cRequestProduct);


    List<Product> getAllProducts();

    List<LResponseProduct> searchProducts(String keyword);

    DResponseProduct getProductById(Long productId);

    @Transactional
    URequestProduct updateProductInfo(URequestProduct uRequestProduct);


    @Transactional
    boolean deleteProduct(Long productId);

    Page<LResponseProduct> Sorting(int page, Sort.Direction direction);

    Page<Product> getAllProducts(int page);

    Page<LResponseProduct> getAllProductDTOs(int page, Sort.Direction direction);

    List<LResponseProduct> getAllProductDiscountByCategory(CategoryC categoryId);

    List<LResponseProduct> getAllProductDiscountByManufacture(Manufacturer manufacturerId);

    List<LResponseProduct> getAllProductDiscountByDiscountPercentage(BigDecimal discountPercentage);
}
