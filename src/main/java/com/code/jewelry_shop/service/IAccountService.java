package com.code.jewelry_shop.service;

import com.code.jewelry_shop.dto.request.authen.ChangePassword;
import com.code.jewelry_shop.dto.response.authen.EmailResult;
import com.code.jewelry_shop.dto.request.authen.Register;
import com.code.jewelry_shop.dto.response.ResponseAccount;
import com.code.jewelry_shop.dto.response.forDetail.DResponseAccount;
import com.code.jewelry_shop.entity.Account;
import com.code.jewelry_shop.security.jwt.JwtAuthResponse;
import com.code.jewelry_shop.utils.constant.AuthoritiesConstants;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Optional;

public interface IAccountService {

    Optional<ResponseAccount> getByUsername(String username);

    JwtAuthResponse checkLogin(String username, String password);

    @Transactional
    DResponseAccount registerAccount(Register register);

    @Transactional
    boolean deleteAccount(Long id);

    List<ResponseAccount> getAllAccount();

    @Transactional
    void changeRole(Long id);

    List<ResponseAccount> getByRole(AuthoritiesConstants role);

    @Transactional
    boolean changeStatus(Long id);

    @Transactional
    void changePassword(ChangePassword changePassword);

    EmailResult sendResetPassword(ChangePassword changePassword);

    @Transactional
    Optional<ResponseAccount> resetPassword(ChangePassword changePassword);



}
