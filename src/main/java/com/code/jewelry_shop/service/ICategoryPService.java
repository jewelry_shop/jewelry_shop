package com.code.jewelry_shop.service;

import com.code.jewelry_shop.entity.CategoryP;
import jakarta.transaction.Transactional;

public interface ICategoryPService {

    @Transactional
    CategoryP createCategory(CategoryP categoryP);

    @Transactional
    CategoryP updateCategoryP(CategoryP categoryP);

    @Transactional
    boolean deleteCategoryP(Long id);

    @Transactional
    CategoryP findCategoryPByName(String name);
}
