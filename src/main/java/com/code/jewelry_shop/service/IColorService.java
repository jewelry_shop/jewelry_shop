package com.code.jewelry_shop.service;


import com.code.jewelry_shop.entity.Color;

import java.util.List;
import java.util.Optional;

public interface IColorService {
    Color createColor(Color productColor);

    List<Color> getAllColor();

    Optional<Color> getColorById(Long colorId);

    Color updateColor(Color color);

    boolean deleteColorById(Long colorId);
}
