package com.code.jewelry_shop.service;

import com.code.jewelry_shop.dto.response.forList.LResponseProduct;
import com.code.jewelry_shop.entity.Manufacturer;
import jakarta.transaction.Transactional;

import java.util.List;

public interface IManufacturerService {

    @Transactional
    Manufacturer createManufacturer(Manufacturer manufacturer);

    @Transactional
    Manufacturer updateManufacturer(Manufacturer manufacturer);

    @Transactional
    boolean deleteManufacturer(Long id);

    List<Manufacturer> getAllManufacturer();

    List<LResponseProduct> getProductsByManufacturer(Manufacturer manufacturer);
}
