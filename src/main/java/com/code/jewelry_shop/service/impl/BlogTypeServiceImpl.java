package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.entity.BlogType;
import com.code.jewelry_shop.repository.BlogTypeRepository;
import com.code.jewelry_shop.service.IBlogTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BlogTypeServiceImpl implements IBlogTypeService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private BlogTypeRepository blogTypeRepository;

    @Override
    @Transactional
    public BlogType createBlogType(BlogType blogType){
        log.debug("Created Blog Type: {}", blogType);
        return blogTypeRepository.save(blogType);
    }

    @Override
    @Transactional
    public boolean deleteBlogType(Long id){
        Optional<BlogType> blogType = blogTypeRepository.findById(id);
        if(blogType.isPresent()){
            blogTypeRepository.deleteById(id);
            log.info("Deleted Blog Type");
            return true;
        }
        log.error("An error occurred when deleting Blog Type");
        return false;
    }

    @Override
    public BlogType getBlogType(Long id){
        return blogTypeRepository.findById(id).orElseThrow();
    }

    @Override
    public List<BlogType> getAll(){
        return blogTypeRepository.findAll();
    }

}
