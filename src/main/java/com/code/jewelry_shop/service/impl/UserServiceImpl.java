package com.code.jewelry_shop.service.impl;


import com.code.jewelry_shop.dto.request.authen.Register;
import com.code.jewelry_shop.dto.request.forUpdate.URequestUser;
import com.code.jewelry_shop.dto.response.ResponseAccount;
import com.code.jewelry_shop.dto.response.forDetail.DResponseUser;
import com.code.jewelry_shop.dto.response.forList.LResponseUser;
import com.code.jewelry_shop.entity.Account;
import com.code.jewelry_shop.entity.Cart;
import com.code.jewelry_shop.entity.User;
import com.code.jewelry_shop.repository.AccountRepository;
import com.code.jewelry_shop.repository.CartRepository;
import com.code.jewelry_shop.repository.UserRepository;
import com.code.jewelry_shop.service.IUserService;
import com.code.jewelry_shop.utils.exception.ApiException;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements IUserService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private FirebaseImageService firebaseImageService;

    @Override
    @Transactional
    public User create(Register register) {
        Optional<Account> accountOptional = accountRepository.findByUsername(register.getUserName());

        if (accountOptional.isPresent()) {
            throw new ApiException("An account already exists with email:{}"+ register.getUserName(), HttpStatus.CONFLICT);
        } else {

            User newUser = new User();
            newUser.setFullName(register.getFullName());
            newUser.setEmail(register.getUserName());
            newUser.setCreateAt(LocalDateTime.now());

            User result = userRepository.save(newUser);

            Cart newCart = new Cart();
            newCart.setUser(newUser);
            cartRepository.save(newCart);


            return result;

        }

    }

    @Override
    @Transactional
    public User update(MultipartFile multipartFile, Long id, URequestUser updateUser){
        try {
            User user = userRepository.findById(id).orElseThrow(() -> new ApiException("User not found", HttpStatus.NOT_FOUND));
            updateUserField(updateUser, user);
            if (multipartFile != null && !areAllFilesEmpty(multipartFile)){
                deleteOldAvatar(user.getId());
                savedAvatar(multipartFile, user);
            }
            User result = userRepository.save(user);
            String imageUrl = result.getAvatar();
            log.debug("Updated User: {}", updateUser);
            return result;

        } catch (NoSuchElementException e) {
            log.error("An error occurred when loading your page");
            throw new ApiException("An error occurred when loading your page", HttpStatus.BAD_REQUEST);
        }
    }
    private boolean areAllFilesEmpty(MultipartFile file) {
        return file.isEmpty();
    }
    private void deleteOldAvatar(Long userId) {
        String imageUrl = userRepository.findById(userId)
                .map(User::getAvatar)
                .orElse(null);
        if (imageUrl != null) {
            String fileName = extractFilenameFromUrl(imageUrl);
            try {
                firebaseImageService.delete(fileName);
            } catch (IOException e) {
                log.error("An error occurred while deleting old avatar for user with ID: {}", userId, e);
            }
        }
    }
    private String extractFilenameFromUrl(String imageUrl) {
        int startIndex = imageUrl.lastIndexOf("/") + 1;
        int endIndex = imageUrl.indexOf("?");
        return imageUrl.substring(startIndex, endIndex);
    }

    private void updateUserField(URequestUser updateUser, User existingUser) {
        modelMapper.map(updateUser, existingUser);
        userRepository.save(existingUser);
    }

    private void savedAvatar(MultipartFile multipartFile, User savedAvatar) {
        if (!multipartFile.isEmpty()) {
            try {
                String imageUrl = firebaseImageService.save(multipartFile);
                savedAvatar.setAvatar(imageUrl);
            } catch (IOException e) {
                log.error("Cannot save Avatar.", e);
            }
        }
    }

    public List<LResponseUser> getAll() {
        List<User> users = userRepository.findAll();
        return users.stream()
                .map(user -> modelMapper.map(user, LResponseUser.class))
                .collect(Collectors.toList());
    }

    @Override
    public DResponseUser getResponseUserByUserId(Long id) {
        User user = userRepository.findById(id).orElseThrow(()-> new ApiException("User not found with id: " + id, HttpStatus.NOT_FOUND));
        modelMapper.typeMap(User.class, DResponseUser.class).addMappings(mapping -> mapping.skip(DResponseUser::setAccountId));
        return modelMapper.map(user, DResponseUser.class);
    }

    @Override
    public DResponseUser getResponseUserByAccountId(Long id) {
        User user = userRepository.findByAccountId(id).orElseThrow(() -> new ApiException("User not found with account_id:" + id, HttpStatus.NOT_FOUND));
        return modelMapper.map(user, DResponseUser.class);
    }

//    private DResponseUser mapUserToResponseUser(User user) {
//        DResponseUser responseUser = new DResponseUser();
//        responseUser.setId(user.getId());
//        responseUser.setFullName(user.getFullName());
//        responseUser.setPhone(user.getPhone());
//        responseUser.setDateOfBirth(user.getDateOfBirth());
//        responseUser.setAvatar(user.getAvatar());
//        responseUser.setCreateAt(user.getCreateAt());
//        responseUser.setAccountId(user.getAccount().getId());
//        return responseUser;
//    }


    @Override
    public User getUserByUserId(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ApiException("User not found with id: " + id, HttpStatus.NOT_FOUND));
    }

    @Override
    public User getUserByAccountId(Long id){
        return userRepository.findByAccountId(id)
                .orElseThrow(() -> new ApiException("User not found with account_id" + id, HttpStatus.NOT_FOUND));
    }

    @Override
    public DResponseUser getUserByPhone(String phone) {
        User user = userRepository.findByPhone(phone);
        if (user == null) {
            throw new ApiException("User not found with phone: " + phone, HttpStatus.NOT_FOUND);
        }
        return modelMapper.map(user, DResponseUser.class);
    }

}


