package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.dto.request.forCreate.CRequestOrder;
import com.code.jewelry_shop.dto.request.forUpdate.URequestOrder;
import com.code.jewelry_shop.dto.response.ResponseOrder;
import com.code.jewelry_shop.dto.response.forList.LResponseOrder;
import com.code.jewelry_shop.dto.response.forList.LResponseUser_Oder;
import com.code.jewelry_shop.entity.DiscountOrder;
import com.code.jewelry_shop.entity.Order;
import com.code.jewelry_shop.entity.OrderDetail;
import com.code.jewelry_shop.entity.User;
import com.code.jewelry_shop.repository.OrderRepository;
import com.code.jewelry_shop.repository.UserRepository;
import com.code.jewelry_shop.service.IDiscountOrderService;
import com.code.jewelry_shop.service.IOrderDetailService;
import com.code.jewelry_shop.service.IOrderService;
import com.code.jewelry_shop.service.IUserService;
import com.code.jewelry_shop.utils.constant.StatusOrder;
import com.code.jewelry_shop.utils.exception.ApiException;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements IOrderService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private IUserService userService;

    @Autowired
    private IDiscountOrderService discountOrderService;

    @Autowired
    private IOrderDetailService orderDetailService;

    @Override
    public Order create(CRequestOrder requestOrder, User user) {
        Order order = new Order();

        if(user.getAccount().getProviderId() != null){
            user.setPhone(requestOrder.getPhone());
            user.setAddress(requestOrder.getAddress());
            userRepository.save(user);
            order.setUser(user);
        }else if (user.getAccount().getId() != null) {
            order.setUser(user);
        }
        else{
            User user1 = userRepository.findById(user.getId()).orElseThrow(() -> new ApiException("User not found with id"+ user.getId(), HttpStatus.NOT_FOUND));
            modelMapper.map(requestOrder, user1);
            userRepository.save(user1);
            order.setUser(user1);
        }
        order.setOrderDate(LocalDateTime.now());
        order.setStatus(StatusOrder.Pending_Confirmation);
        calculateTotalPrice(order);

        return orderRepository.save(order);
    }


    @Override
    public List<LResponseUser_Oder> getAll(){

        List<Order> orders = orderRepository.findAll();
        List<User> users = userRepository.findAll();
        List<LResponseUser_Oder> lResponseUser_orders = new ArrayList<>();

        for (User user : users) {
            LResponseUser_Oder responseUser_oder = new LResponseUser_Oder();
            modelMapper.map(user, responseUser_oder);

            List<LResponseOrder> userOrders = new ArrayList<>();

            for (Order order : orders) {
                if (Objects.equals(order.getUser().getId(), user.getId())) {
                    LResponseOrder responseOrder = new LResponseOrder();
                    modelMapper.map(order, responseOrder);
                    userOrders.add(responseOrder);
                }
            }

            responseUser_oder.setLResponseOrder(userOrders);
            lResponseUser_orders.add(responseUser_oder);
        }

        return lResponseUser_orders;
    }


    @Override
    public Order getById(Long id) {
        return orderRepository.findById(id).orElseThrow(() -> new ApiException("Order not found with id"+ id, HttpStatus.NOT_FOUND));
    }

    @Override
    public List<ResponseOrder> getAllByEmailContains(String email) {
        List<Order> orders = orderRepository.findByUserId_EmailContains(email);

        return orders.stream()
                .map(order -> modelMapper.map(order, ResponseOrder.class))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Order update(URequestOrder uRequestOrder){
        Order orders = orderRepository.findById(uRequestOrder.getId()).orElseThrow(() -> new ApiException("Order not found with id: {}" + uRequestOrder.getId(), HttpStatus.NOT_FOUND));
        User user = userService.getUserByUserId(orders.getUser().getId());
        modelMapper.map(uRequestOrder, user);
        userRepository.save(user);
        orders.setStatus(uRequestOrder.getStatus());
        return orderRepository.save(orders);
    }



    @Override
    @Transactional
    public Order applyDiscount(Order order, String discountCode) {

        DiscountOrder discount = discountOrderService.getDiscountOrderByCode(discountCode);
        if (discount != null) {
            order.getDiscountOrder().setId(discount.getId());
        } else {
            throw new ApiException("Discount code not found");
        }
        return calculateTotalPrice(order);
    }

    @Override
    @Transactional
    public boolean confirmPayment(Long orderId, String paymentName, Boolean success){
        Optional<Order> optionalOrder = orderRepository.findById(orderId);
        if (optionalOrder.isPresent()) {
            Order order = optionalOrder.get();

            if ("Cash".equalsIgnoreCase(paymentName)) {
                order.setStatus(StatusOrder.In_Transit);
            } else {
                order.setStatus(StatusOrder.Confirmed);
            }

            orderRepository.save(order);
            return true;
        } else {
            throw new ApiException("Order not found with id: " + orderId, HttpStatus.NOT_FOUND);
        }
    }


    private Order calculateTotalPrice(Order order) {
        BigDecimal totalPrice = BigDecimal.ZERO;
        List<OrderDetail> orderDetails = orderDetailService.getOrderDetailsByOrderId(order.getId());
        for (OrderDetail orderDetail : orderDetails) {
            totalPrice = totalPrice.add(orderDetail.getSubTotalPrice());
        }

        if (order.getDiscountOrder().getId() != null) {
            DiscountOrder discount = discountOrderService.getDiscountOrderById(order.getDiscountOrder().getId());
            if (conditionsMet(discount, order.getId())) {
                BigDecimal discountedPercentage = BigDecimal.ONE.subtract(discount.getDiscountedPercentage());
                BigDecimal discountedTotalPrice = totalPrice.multiply(discountedPercentage);
                totalPrice = discountedTotalPrice.setScale(2, RoundingMode.HALF_UP);
            }
        }

        order.setTotalPrice(totalPrice);
        return order;
    }


    private boolean conditionsMet(DiscountOrder discountOrder, Long orderId) {

        if (discountOrder == null || orderId == null) {
            return false;
        }

        Order order = orderRepository.findById(orderId).orElseThrow();

        BigDecimal minPurchaseAmount = discountOrder.getConditions();
        Integer maxUsage = discountOrder.getMaxUsage();
        Integer currentUsage = discountOrder.getCurrentUsage();
        LocalDate currentDate = LocalDate.now();

        if (currentUsage < maxUsage) {
            if (discountOrder.getVoucher() != null) {
                LocalDate validFrom = discountOrder.getVoucher().getValidFrom();
                LocalDate validTo = discountOrder.getVoucher().getValidTo();
                if (currentDate.isBefore(validFrom) || currentDate.isAfter(validTo)) {
                    return false;
                }
            } else if (discountOrder.getDiscountCode() != null) {
                LocalDate expiredDate = discountOrder.getDiscountCode().getExpiredDate();
                if (currentDate.isAfter(expiredDate)) {
                    return false;
                }
            }

            return order.getTotalPrice().compareTo(minPurchaseAmount) >= 0;
        }

        return false;


    }
}
