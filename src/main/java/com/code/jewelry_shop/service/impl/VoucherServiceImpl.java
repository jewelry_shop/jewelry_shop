package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.dto.request.forCreate.CRequestVoucher;
import com.code.jewelry_shop.dto.request.forUpdate.URequestVoucher;
import com.code.jewelry_shop.entity.Voucher;
import com.code.jewelry_shop.repository.VoucherRepository;
import com.code.jewelry_shop.service.IVoucherService;
import com.code.jewelry_shop.utils.exception.ApiException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Service
public class VoucherServiceImpl implements IVoucherService {

    @Autowired
    private VoucherRepository voucherRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public Voucher create(CRequestVoucher cRequestVoucher){

        validateVoucherRequest(cRequestVoucher);

        Voucher voucher = new Voucher();

        modelMapper.map(cRequestVoucher, voucher);

        return voucherRepository.save(voucher);
    }

    @Override
    @Transactional
    public Voucher update(URequestVoucher uRequestVoucher) {

        Voucher voucher = voucherRepository.findById(uRequestVoucher.getId()).orElseThrow(() -> new ApiException("Voucher not found with id: {}"+ uRequestVoucher.getId(), HttpStatus.NOT_FOUND));

        modelMapper.map(uRequestVoucher, voucher);

        return voucherRepository.save(voucher);
    }

    @Override
    @Transactional
    public void delete(Long id){
        Voucher voucher = voucherRepository.findById(id).orElseThrow(() -> new ApiException("Voucher not found with id: {}" + id, HttpStatus.NOT_FOUND));
        voucherRepository.delete(voucher);
    }

    @Override
    public Voucher getVoucherByCode(String name) {
        return voucherRepository.findByVoucherCode(name).orElseThrow(() -> new ApiException("Voucher doesn't exist", HttpStatus.NOT_FOUND));
    }




    

//    @Override
//    public List<LResponseVoucher> search(String s) {
//        List<Voucher> voucherList = new ArrayList<Voucher>(voucherRepository.findAllByVoucherNameContains(s));
//        List<LResponseVoucher> lResponseVouchers = new ArrayList<LResponseVoucher>();
//        List<DiscountOrder> discountOrder = discountOrderRepository.findAll();
//        for(int i=0; i<voucherList.size(); i++){
//            Voucher a = voucherList.get(i);
//            LResponseVoucher lResponseVoucher = new LResponseVoucher();
//            lResponseVoucher.setId(a.getId());
//            lResponseVoucher.setVoucherName(a.getVoucherName());
//            lResponseVoucher.setVoucherCode(a.getVoucherCode());
//            lResponseVoucher.setValid_from(a.getValid_from());
//            lResponseVoucher.setValid_to(a.getValid_to());
//            lResponseVoucher.setConditions(discountOrder.);
//            lResponseVoucher.setMaxUsage(discountOrder.);
//            lResponseVoucher.setCurrentUsage(discountOrder.);
//            lResponseVoucher.setDiscountPercentage(discountOrder.);
//            lResponseVouchers.add(lResponseVoucher);
//        }
//        return lResponseVouchers;
//    }

    private void validateVoucherRequest(CRequestVoucher cRequestVoucher) {
        LocalDate currentDate = LocalDate.now();
        if (currentDate.isBefore(cRequestVoucher.getValidFrom())&&cRequestVoucher.getValidFrom() == null || cRequestVoucher.getValidTo() == null || currentDate.isAfter(cRequestVoucher.getValidTo())) {
            throw new ApiException("Invalid validity period for voucher.", HttpStatus.BAD_REQUEST);
        }

        if (voucherRepository.existsByVoucherNameOrVoucherCode(cRequestVoucher.getVoucherName(), cRequestVoucher.getVoucherCode())) {
            throw new ApiException("Voucher with name '" + cRequestVoucher.getVoucherName() + "' or Voucher with code '" + cRequestVoucher.getVoucherCode() +"' already exists.", HttpStatus.CONFLICT);
        }

    }

}

