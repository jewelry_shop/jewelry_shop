package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.entity.Color;
import com.code.jewelry_shop.repository.ColorRepository;
import com.code.jewelry_shop.service.IColorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ColorServiceImpl implements IColorService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired private ColorRepository colorRepository;
    @Override
    public Color createColor(Color color) {
        log.debug("Created Color: {}", color);
        return colorRepository.save(color);
    }

    @Override
    public List<Color> getAllColor() {
        return colorRepository.findAll();
    }

    @Override
    public Optional<Color> getColorById(Long colorId) {
        return colorRepository.findById(colorId);
    }
    @Override
    public Color updateColor(Color color) {
        log.debug("Updated Color: {}", color);
        return colorRepository.save(color);
    }
    @Override
    public boolean deleteColorById(Long colorId) {
        if (colorRepository.existsById(colorId)) {
            colorRepository.deleteById(colorId);
            log.info("Deleted Color");
            return true;
        }
        log.error("An error occurred when deleting Color");
        return false;
    }
}
