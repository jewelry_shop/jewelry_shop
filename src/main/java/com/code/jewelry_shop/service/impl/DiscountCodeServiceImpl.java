package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.dto.request.forCreate.CRequestDiscountCode;
import com.code.jewelry_shop.dto.request.forUpdate.URequestDiscountCode;
import com.code.jewelry_shop.entity.DiscountCode;
import com.code.jewelry_shop.repository.DiscountCodeRepository;
import com.code.jewelry_shop.service.IDiscountCodeService;
import com.code.jewelry_shop.utils.exception.ApiException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Random;

@Service
public class DiscountCodeServiceImpl implements IDiscountCodeService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private DiscountCodeRepository discountCodeRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public DiscountCode create(CRequestDiscountCode cRequestDiscountCode){

        validateDiscountCodeRequest(cRequestDiscountCode);

        DiscountCode discountCode = new DiscountCode();

        modelMapper.map(cRequestDiscountCode, discountCode);

        String generateCode;

        do{
            generateCode = generateDiscountCode();
            discountCode.setDiscountCode(generateCode);
        }while(!generateCode.equals(discountCode.getDiscountCode()));

        return discountCodeRepository.save(discountCode);
    }

    @Override
    @Transactional
    public DiscountCode update(URequestDiscountCode uRequestDiscountCode) {

        DiscountCode discountCode = discountCodeRepository.findById(uRequestDiscountCode.getId()).orElseThrow(() -> new ApiException("DiscountCode not found with id: {}"+ uRequestDiscountCode.getId(), HttpStatus.NOT_FOUND));

        modelMapper.map(uRequestDiscountCode, discountCode);

        return discountCodeRepository.save(discountCode);
    }

    @Override
    @Transactional
    public void delete(Long id){
        DiscountCode discountCode = discountCodeRepository.findById(id).orElseThrow(() -> new ApiException("DiscountCode not found with id: {}"+ id, HttpStatus.NOT_FOUND));
        discountCodeRepository.delete(discountCode);
    }

    @Override
    public DiscountCode getDiscountCodeByCode(String code) {
        return discountCodeRepository.findByDiscountCode(code).orElseThrow(() -> new ApiException("DiscountCode doesn't exist", HttpStatus.NOT_FOUND));
    }



    private void validateDiscountCodeRequest(CRequestDiscountCode cRequestDiscountCode) {
        LocalDate currentDate = LocalDate.now();

        if (currentDate.isBefore(cRequestDiscountCode.getExpiredDate())&&cRequestDiscountCode.getExpiredDate()==null) {
            throw new ApiException("Invalid validity period for DiscountCode.", HttpStatus.BAD_REQUEST);
        }

        if (discountCodeRepository.existsByDiscountCodeName(cRequestDiscountCode.getDiscountCodeName())) {
            throw new ApiException("DiscountCode with name '" + cRequestDiscountCode.getDiscountCodeName() +"' already exists.", HttpStatus.CONFLICT);
        }

    }


    private String generateDiscountCode(){
        int codeLength = 9;
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();

        StringBuilder sb = new StringBuilder(codeLength);
        for (int i = 0; i < codeLength; i++){
            sb.append(characters.charAt(random.nextInt(characters.length())));
        }
        return sb.toString();
    }
}
