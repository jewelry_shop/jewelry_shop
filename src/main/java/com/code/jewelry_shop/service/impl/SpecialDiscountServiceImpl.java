package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.dto.request.forCreate.CRequestSpecialDiscount;
import com.code.jewelry_shop.entity.SpecialDiscount;
import com.code.jewelry_shop.repository.SpecialDiscountRepository;
import com.code.jewelry_shop.service.IProductService;
import com.code.jewelry_shop.service.ISpecialDiscountService;
import com.code.jewelry_shop.utils.exception.ApiException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpecialDiscountServiceImpl implements ISpecialDiscountService {

    @Autowired
    public SpecialDiscountRepository specialDiscountRepository;

    @Autowired
    public ModelMapper modelMapper;

    @Autowired
    IProductService productService;

    @Override
    public SpecialDiscount insert(CRequestSpecialDiscount cRequestSpecialDiscount) {
        SpecialDiscount specialDiscount = new SpecialDiscount();
        modelMapper.map(cRequestSpecialDiscount, specialDiscount);
        return specialDiscountRepository.save(specialDiscount);
    }

    @Override
    public SpecialDiscount getOne(Long id) {
        return specialDiscountRepository.findById(id).orElseThrow(() -> new ApiException("SpecialDiscount not found with id: {}"+ id, HttpStatus.NOT_FOUND));
    }

    @Override
    public List<SpecialDiscount> getAll() {
        return specialDiscountRepository.findAll();
    }

    @Override
    public SpecialDiscount update(SpecialDiscount discountCombo) {
        return specialDiscountRepository.save(discountCombo);
    }

    @Override
    public List<SpecialDiscount> search(String s) {
        return specialDiscountRepository.findAllByDiscountNameContains(s);
    }
}
