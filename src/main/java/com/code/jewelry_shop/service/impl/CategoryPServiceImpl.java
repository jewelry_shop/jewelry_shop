package com.code.jewelry_shop.service.impl;


import com.code.jewelry_shop.repository.CategoryPRepository;
import com.code.jewelry_shop.service.ICategoryPService;
import com.code.jewelry_shop.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoryPServiceImpl  implements ICategoryPService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private CategoryPRepository categoryPRepository;

    @Override
    public CategoryP findCategoryPByName(String categoryPName) {
        return categoryPRepository.findByCategoryPName(categoryPName);
    }
    @Override
    @Transactional
    public CategoryP createCategory(CategoryP categoryP){
        log.debug("Created CategoryP: {}", categoryP);
        return categoryPRepository.save(categoryP);
    }

    @Override
    @Transactional
    public CategoryP updateCategoryP(CategoryP categoryP){
        log.debug("Created CategoryP: {}", categoryP);
        return categoryPRepository.save(categoryP);
    }

    @Override
    @Transactional
    public boolean deleteCategoryP(Long categoryId){
        if(categoryPRepository.existsById(categoryId)){
            categoryPRepository.deleteById(categoryId);
            log.info("CategoryP deleted successfully");
            return true;
        }
        log.error("Category with ID {} not found, deletion failed", categoryId);
        return false;
    }
}