package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.entity.Payment;
import com.code.jewelry_shop.repository.PaymentRepository;
import com.code.jewelry_shop.service.IPaymentService;
import com.code.jewelry_shop.utils.exception.ApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PaymentServiceImpl implements IPaymentService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public Payment createPayment(Payment payment){
        try{
            log.debug("Created Payment: {}", payment);
            return paymentRepository.save(payment);
        }
        catch(Exception e) {
            log.error("An error occurred when creating Payment", e);
            throw new ApiException("Failed to add payment");
        }
    }
    @Override
    public List<Payment> getAllPayment(){
        return paymentRepository.findAll();
    }
    @Override
    public Optional<Payment> getPaymentById(Long id){

        return paymentRepository.findById(id);
    }
    @Override
    public Payment updatePayment(Payment payment){
        return paymentRepository.save(payment);
    }
    @Override
    public boolean deletePaymentByTd(Long id){
        if(paymentRepository.existsById(id)){
            paymentRepository.deleteById(id);
            log.debug("Deleted Payment");
            return true;
        }
        log.error("An error occurred when deleting Payment");
        return false;
    }
}
