package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.dto.request.authen.ChangePassword;
import com.code.jewelry_shop.dto.response.authen.EmailResult;
import com.code.jewelry_shop.dto.request.authen.Register;
import com.code.jewelry_shop.dto.response.ResponseAccount;
import com.code.jewelry_shop.dto.response.forDetail.DResponseAccount;
import com.code.jewelry_shop.entity.Account;
import com.code.jewelry_shop.entity.Role;
import com.code.jewelry_shop.entity.User;
import com.code.jewelry_shop.repository.AccountRepository;
import com.code.jewelry_shop.repository.RoleRepository;
import com.code.jewelry_shop.repository.UserRepository;
import com.code.jewelry_shop.security.jwt.JwtAuthResponse;
import com.code.jewelry_shop.security.jwt.JwtUtil;
import com.code.jewelry_shop.service.IAccountService;
import com.code.jewelry_shop.service.IUserService;
import com.code.jewelry_shop.utils.VerificationCodeManager;
import com.code.jewelry_shop.utils.constant.AuthProvider;
import com.code.jewelry_shop.utils.constant.AuthoritiesConstants;
import com.code.jewelry_shop.utils.constant.StatusAccount;
import com.code.jewelry_shop.utils.exception.ApiException;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements IAccountService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private IUserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private ModelMapper modelMapper;

    @Value("${spring.mail.username")
    private String sender;

    @Autowired
    private FirebaseImageService firebaseImageService;

    public Optional<ResponseAccount> getByUsername(String username) {
        Optional<Account> accountOptional = accountRepository.findByUsername(username);
        return accountOptional.map(this::convertToResponseAccount);
    }

    @Override
    public JwtAuthResponse checkLogin(String username, String password){

        Account account = accountRepository.findByUsername(username).orElseThrow(() -> new ApiException("Username not found with username: {}"+ username, HttpStatus.NOT_FOUND));
        if (account.getStatusAccount() != StatusAccount.ENABLED) {
            throw new ApiException("Account is not active", HttpStatus.FORBIDDEN);
        }

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(username, password);
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = "Bearer " + jwtUtil.generateJwtToken(authentication);


        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .toList();

        ResponseAccount accountResponse = modelMapper.map(account, ResponseAccount.class);
        accountResponse.setRole(roles.toString());
        return new JwtAuthResponse(jwt, accountResponse);
    }

    @Override
    @Transactional
    public DResponseAccount registerAccount(Register register){
        try{
            accountRepository.findByUsername(register.getUserName())
                    .ifPresent(user -> {
                        throw new ApiException("Username already exists with username: {}"+ register.getFullName(), HttpStatus.CONFLICT);
                    });
            Optional<Role> roles = roleRepository.findByRoleName(AuthoritiesConstants.ROLE_CUSTOMER);
            Set<Role> authorities = new HashSet<>();
            Account newAccount = new Account();
            newAccount.setUsername(register.getUserName());
            newAccount.setPassword(bCryptPasswordEncoder.encode(register.getPassword()));
            newAccount.setProvider(AuthProvider.local);
            roles.ifPresent(authorities::add);
            newAccount.setRole(authorities);
            newAccount.setStatusAccount(StatusAccount.ENABLED);
            accountRepository.save(newAccount);
            userService.create(register);

            DResponseAccount dResponseAccount = new DResponseAccount();
            log.debug("Created Information for Account: {}", newAccount);
            return dResponseAccount;
        }catch(Exception e){
            log.error("Unexpected error occurred while registering account", e);
            throw new ApiException("An unexpected error occurred");
        }

    }

    @Override
    @Transactional
    public boolean deleteAccount(Long id) {
        Optional<Account> optionalAccount = accountRepository.findById(id);
        if(optionalAccount.isPresent()){
            User userId = userService.getUserByAccountId(id);
            userRepository.deleteByAccountId(id);
//            accountRepository.deleteById(id);
            deleteAvatar(userId.getId());
            return true;
        }
        log.error("Cannot delete Account. Account with id {} not found.", id);
        return false;
    }

    @Override
    public List<ResponseAccount> getAllAccount() {
        List<Account> accounts = accountRepository.findAll();
        List<ResponseAccount> responseAccounts = new ArrayList<>();

        for (Account account : accounts) {
            ResponseAccount responseAccount = modelMapper.map(account, ResponseAccount.class);
            responseAccounts.add(responseAccount);
        }

        return responseAccounts;
    }



    private void deleteAvatar(Long userId) {
        String imageUrl = userRepository.findById(userId)
                .map(User::getAvatar)
                .orElse(null);
        if (imageUrl != null) {
            String fileName = extractFilenameFromUrl(imageUrl);
            try {
                firebaseImageService.delete(fileName);
            } catch (IOException e) {
                log.error("An error occurred while deleting avatar for user with ID: {}", userId, e);
                throw new ApiException("An error occurred while deleting avatar");
            }
        }
    }

    private String extractFilenameFromUrl(String imageUrl) {
        int startIndex = imageUrl.lastIndexOf("/") + 1;
        int endIndex = imageUrl.indexOf("?");
        return imageUrl.substring(startIndex, endIndex);
    }

    @Override
    @Transactional
    public void changeRole(Long id) {
        Account account = accountRepository.findById(id).orElseThrow(() -> new ApiException("Username not found", HttpStatus.NOT_FOUND));
        Set<Role> currentAuthorities = account.getRole();

        if(currentAuthorities.contains(roleRepository.findByRoleName(AuthoritiesConstants.ROLE_CUSTOMER).orElse(null))){
            currentAuthorities.clear();
            currentAuthorities.add(roleRepository.findByRoleName(AuthoritiesConstants.ROLE_MANAGER).orElse(null));
        }
        else {
            currentAuthorities.clear();
            currentAuthorities.add(roleRepository.findByRoleName(AuthoritiesConstants.ROLE_CUSTOMER).orElse(null));
        }
        accountRepository.save(account);
    }

    @Override
    public List<ResponseAccount> getByRole(AuthoritiesConstants role) {
        Set<Role> roles = new HashSet<>();
        roles.add(new Role(role));

        List<Account> accounts = accountRepository.findAllByRole(roles);
        List<ResponseAccount> responseAccounts = new ArrayList<>();

        for (Account account : accounts) {
            ResponseAccount responseAccount = modelMapper.map(account, ResponseAccount.class);
            responseAccounts.add(responseAccount);
        }

        return responseAccounts;
    }

    @Override
    @Transactional
    public boolean changeStatus(Long id) {
        Optional<Account> optionalAccount = accountRepository.findById(id);
        if (optionalAccount.isPresent()) {
            Account account = optionalAccount.get();
            StatusAccount currentStatusAccount = account.getStatusAccount();
            StatusAccount newStatusAccount = (currentStatusAccount == StatusAccount.ENABLED) ? StatusAccount.LOCKED : StatusAccount.ENABLED;
            account.setStatusAccount(newStatusAccount);
            accountRepository.save(account);
            return true;
        } else {
            log.error("Cannot change status. Account with id {} not found.", id);
            return false;
        }
    }

    @Override
    @Transactional
    public void changePassword(ChangePassword changePassword){
        Optional<Account> accountOptional = accountRepository.findByUsername(changePassword.getUsername());
        if(accountOptional.isPresent()){
            Account account = accountRepository.findById(accountOptional.get().getId()).orElseThrow(() -> new ApiException("Account not found", HttpStatus.NOT_FOUND));
            if(!bCryptPasswordEncoder.matches(changePassword.getCurrentPassword(), account.getPassword())){
                throw new ApiException("Password doesn't match.");
            }
            account.setPassword(bCryptPasswordEncoder.encode(changePassword.getNewPassword()));
            accountRepository.save(account);

        }
        log.error("Account doesn't exist");
        throw new ApiException("Account {} doesn't exist."+ changePassword.getUsername(), HttpStatus.NOT_FOUND);
    }

    @Override
    public EmailResult sendResetPassword(ChangePassword changePassword) {
        Optional<Account> account = accountRepository.findByUsername(changePassword.getUsername());
        if(account.isPresent()){
            String verificationCode = generateVerificationCode();
            VerificationCodeManager.saveVerificationCode(changePassword.getUsername(), verificationCode);
            sendEmail(changePassword.getUsername(), "Reset Password","Your verification code is: " + verificationCode);
            return new EmailResult(true, "Send reset email successfully!");
        }
        log.error("Username doesn't exist: {}", changePassword.getUsername());
        throw new ApiException("Username doesn't exist.", HttpStatus.NOT_FOUND);
    }

    private String generateVerificationCode(){
        int codeLength = 6;
        String allowedChars = "0123456789";
        Random random = new Random();

        StringBuilder verificationCode = new StringBuilder(codeLength);
        for(int i = 0; i<codeLength; i++){
            int randomIndex = random.nextInt(allowedChars.length());
            char randomChar = allowedChars.charAt(randomIndex);
            verificationCode.append(randomChar);
        }
        return verificationCode.toString();
    }

    public void sendEmail(String email, String subject, String content){
        try{
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(sender);
            message.setTo(email);
            message.setSubject(subject);
            message.setText(content);
            javaMailSender.send(message);
        }
        catch (MailException e){
            log.error("An error occurred while sending email with Email: {}", email, e);
            throw new ApiException("Fail to send email: " + e.getMessage());
        }
    }

    @Override
    @Transactional
    public Optional<ResponseAccount> resetPassword(ChangePassword changePassword) {
        boolean isCodeValid = VerificationCodeManager.isVerificationCodeValid(changePassword.getUsername(), changePassword.getVerificationCode());
        if(isCodeValid){
            try{
                Optional<Account> dResponseAccount = accountRepository.findByUsername(changePassword.getUsername());
                if(dResponseAccount.isPresent()){
                    Account account = accountRepository.findById(dResponseAccount.get().getId()).orElseThrow(() -> new ApiException("Account not found", HttpStatus.NOT_FOUND));
                    account.setPassword(bCryptPasswordEncoder.encode(changePassword.getNewPassword()));
                    accountRepository.save(account);
                    sendEmail(changePassword.getUsername(), "Reset password successfully!", "Your password has been reset.");
                    VerificationCodeManager.removeVerificationCode(changePassword.getUsername());
                    return dResponseAccount.map(this::convertToResponseAccount);
                }
                log.error("Username doesn't exist: {}", changePassword.getUsername());
                throw new ApiException("Email doesn't exist.", HttpStatus.NOT_FOUND);
            }catch (Exception e){
                log.error("An error occurred while resetting password with Email: {}", changePassword.getUsername(), e);
                throw new ApiException("Error occurs when resetting password.");
            }
        }
        else {
            throw new ApiException("Verification Code is not valid.");
        }
    }

    private ResponseAccount convertToResponseAccount(Account account) {
        return modelMapper.map(account, ResponseAccount.class);
    }

}
