package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.dto.request.forCreate.CRequestWarranty;
import com.code.jewelry_shop.entity.Warranty;
import com.code.jewelry_shop.repository.WarrantyRepository;
import com.code.jewelry_shop.service.IWarrantyService;
import com.code.jewelry_shop.utils.exception.ApiException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Random;

@Service
public class WarrantyServiceImpl implements IWarrantyService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    public WarrantyRepository warrantyRepository;

    @Autowired
    private ModelMapper modelMapper;


    @Override
    @Transactional
    public Warranty createWarranty(CRequestWarranty cRequestWarranty){
        validateWarrantyRequest(cRequestWarranty);
        Warranty warranty = new Warranty();
        modelMapper.map(cRequestWarranty, warranty);
        String generateCode;
        do{
            generateCode = generateSeries();
            warranty.setSeries(generateCode);
        }while(!generateCode.equals(warranty.getSeries()));
        log.debug("Created Warranty: {}", cRequestWarranty);
        return warrantyRepository.save(warranty);
    }

    @Override
    public Warranty findBySerialNumber(String serialNumber) {
        return warrantyRepository.findBySeries(serialNumber);
    }

    @Override
    @Transactional
    public void claimWarranty(String serialNumber){

        Warranty warranty = findBySerialNumber(serialNumber);
        if (warranty == null) {
            throw new ApiException("Warranty not found with serial number: {}" + serialNumber, HttpStatus.NOT_FOUND);
        }

        LocalDate currentDate = LocalDate.now();
        if (currentDate.isAfter(warranty.getExpiredWarrantyDate())) {
            throw new ApiException("Warranty expired: ");
        }

        warranty.setStatus("Requested warranty");
        warranty.setRequestDate(LocalDateTime.now());
        warrantyRepository.save(warranty);
    }

    private void validateWarrantyRequest(CRequestWarranty cRequestWarranty) {
        LocalDate currentDate = LocalDate.now();

        if (currentDate.isBefore(cRequestWarranty.getExpiredWarrantyDate())&&cRequestWarranty.getExpiredWarrantyDate()==null) {
            throw new ApiException("Invalid validity period for Warranty.", HttpStatus.BAD_REQUEST);
        }

    }

    private String generateSeries(){
        int codeLength = 9;
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();

        StringBuilder sb = new StringBuilder(codeLength);
        for (int i = 0; i < codeLength; i++){
            sb.append(characters.charAt(random.nextInt(characters.length())));
        }
        return sb.toString();
    }
}
