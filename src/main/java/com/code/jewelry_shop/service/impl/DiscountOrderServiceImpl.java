package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.dto.request.forCreate.CRequestDiscountOrder;
import com.code.jewelry_shop.dto.request.forUpdate.URequestDiscountOrder;
import com.code.jewelry_shop.dto.response.ResponseDiscountOrder;
import com.code.jewelry_shop.entity.*;
import com.code.jewelry_shop.repository.DiscountOrderRepository;
import com.code.jewelry_shop.repository.OrderRepository;
import com.code.jewelry_shop.service.IDiscountCodeService;
import com.code.jewelry_shop.service.IDiscountOrderService;
import com.code.jewelry_shop.service.IVoucherService;
import com.code.jewelry_shop.utils.constant.DiscountType;
import com.code.jewelry_shop.utils.exception.ApiException;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class DiscountOrderServiceImpl implements IDiscountOrderService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private DiscountOrderRepository discountOrderRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private IVoucherService iVoucherService;

    @Autowired
    private IDiscountCodeService iDiscountCodeService;


    @Override
    @Transactional
    public DiscountOrder createDiscountOrder(CRequestDiscountOrder cRequestDiscountOrder) {

        if(DiscountType.DISCOUNT_CODE == cRequestDiscountOrder.getDiscountType()){
            iDiscountCodeService.create(cRequestDiscountOrder.getDiscountCode());
        }
        else {
            iVoucherService.create(cRequestDiscountOrder.getVoucher());
        }

        DiscountOrder discountOrder = modelMapper.map(cRequestDiscountOrder, DiscountOrder.class);
        log.debug("Created DiscountOrder: {}", cRequestDiscountOrder);
        return discountOrderRepository.save(discountOrder);
    }

    @Override
    @Transactional
    public DiscountOrder updateDiscountOrder(Long discountOrderId, URequestDiscountOrder uRequestDiscountOrder){
        DiscountOrder discountOrder = discountOrderRepository.findById(discountOrderId).orElseThrow(() -> new ApiException("DiscountOrder not found with id: {} " + discountOrderId, HttpStatus.NOT_FOUND));

        modelMapper.map(uRequestDiscountOrder, discountOrder);

        if(discountOrder.getDiscountType() == DiscountType.VOUCHER){
            iVoucherService.update(uRequestDiscountOrder.getURequestVoucher());
        }
        else if (discountOrder.getDiscountType() == DiscountType.DISCOUNT_CODE) {
            iDiscountCodeService.update(uRequestDiscountOrder.getURequestDiscountCode());
        }

        log.debug("Updated DiscountOrder: {}", uRequestDiscountOrder);
        return discountOrderRepository.save(discountOrder);
    }

    @Override
    @Transactional
    public boolean deleteDiscountOrder(Long discountOrderId) {
        if(discountOrderRepository.existsById(discountOrderId)) {
            DiscountOrder existingDiscountOrder = discountOrderRepository.findById(discountOrderId)
                    .orElseThrow(() -> new ApiException("DiscountOrder not found with id: " + discountOrderId));
            if (existingDiscountOrder.getDiscountType() == DiscountType.DISCOUNT_CODE) {
                iDiscountCodeService.delete(existingDiscountOrder.getDiscountCode().getId());
            } else if (existingDiscountOrder.getDiscountType() == DiscountType.VOUCHER) {
                iVoucherService.delete(existingDiscountOrder.getVoucher().getId());
            }
            log.info("Deleted DiscountOrder");
            discountOrderRepository.delete(existingDiscountOrder);
            return true;
        }
        log.error("An error occurred when deleting DiscountOrder");
        return false;
    }

    @Override
    public List<DiscountOrder> getAllDiscountOrders() {
        return discountOrderRepository.findAll();
    }

    @Override
    public List<DiscountOrder> getAllDiscountOrdersByDiscountType(DiscountType discountType){
        return discountOrderRepository.findByDiscountType(discountType);
    }

    @Override
    public DiscountOrder getDiscountOrderById(Long discountOrderId) {
        return discountOrderRepository.findById(discountOrderId).orElseThrow(() -> new ApiException("DiscountOrder not found with id : {}"+ discountOrderId, HttpStatus.NOT_FOUND));
    }

    @Override
    public List<ResponseDiscountOrder> getDiscountOrderByOrders(List<Long> ordersId) {

        List<ResponseDiscountOrder> responseDiscountOrders = new ArrayList<>();

        for(Long orderId : ordersId){
            Order order = orderRepository.findById(orderId).orElse(null);
            if (order != null) {
                List<DiscountOrder> discountOrders = discountOrderRepository.findByOrdersContaining(order);
                ResponseDiscountOrder responseDiscountOrder = new ResponseDiscountOrder();
                responseDiscountOrder.setOrderId(orderId);
                if (!discountOrders.isEmpty()) {
                    DiscountOrder discountOrder = discountOrders.get(0);
                    responseDiscountOrder.setDiscountedPercentage(discountOrder.getDiscountedPercentage());
                }
                responseDiscountOrders.add(responseDiscountOrder);
            }
        }
        return responseDiscountOrders;
    }

    @Override
    public DiscountOrder getDiscountOrderByCode(String code){

        DiscountCode discountCode = iDiscountCodeService.getDiscountCodeByCode(code);
        Voucher voucher = iVoucherService.getVoucherByCode(code);
        DiscountOrder discountOrder = new DiscountOrder();
        if(discountCode != null){
            discountOrder = discountOrderRepository.findByDiscountCode_Id(discountCode.getId());
        }
        else if(voucher != null){
            discountOrder = discountOrderRepository.findByVoucher_Id(voucher.getId());
        }
        return discountOrder;
    }

}

