package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.dto.response.forList.LResponseProduct;
import com.code.jewelry_shop.repository.ManufacturerRepository;
import com.code.jewelry_shop.repository.ProductImageRepository;
import com.code.jewelry_shop.repository.ProductRepository;
import com.code.jewelry_shop.service.IManufacturerService;
import com.code.jewelry_shop.entity.*;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ManufacturerServiceImpl implements IManufacturerService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductImageRepository productImageRepository;

    @Autowired
    private ManufacturerRepository manufacturerRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    @Transactional
    public Manufacturer createManufacturer(Manufacturer manufacturer){
        log.debug("Created Manufacturer: {}", manufacturer);
        return manufacturerRepository.save(manufacturer);
    }

    @Override
    @Transactional
    public Manufacturer updateManufacturer(Manufacturer manufacturer){
        log.debug("Updated Manufacturer: {}", manufacturer);
        return manufacturerRepository.save(manufacturer);
    }

    @Override
    @Transactional
    public boolean deleteManufacturer(Long id){
        if(manufacturerRepository.existsById(id)){
            manufacturerRepository.deleteById(id);
            log.info("Deleted Manufacturer");
            return true;
        }
        log.error("An error occurred when deleting Manufacturer");
        return false;
    }

    @Override
    public List<Manufacturer> getAllManufacturer()
    {
        return manufacturerRepository.findAll();
    }

    @Override
    public List<LResponseProduct> getProductsByManufacturer(Manufacturer manufacturer) {
        List<Product> products = productRepository.findByManufacturer(manufacturer);
        List<LResponseProduct> lProductDTOs = new ArrayList<>();
        for (Product product : products) {
            LResponseProduct lProductDTO = new LResponseProduct();
            modelMapper.map(product, LResponseProduct.class);
            lProductDTO.setImageLinks(getFirstImageLink(product));
            lProductDTOs.add(lProductDTO);
        }
        return lProductDTOs;
    }
    private String getFirstImageLink(Product product) {
        List<ProductImage> productImages = productImageRepository.findByProductId(product.getId());
        if (!productImages.isEmpty()) {
            return productImages.get(0).getLink();
        }
        return null;
    }

}
