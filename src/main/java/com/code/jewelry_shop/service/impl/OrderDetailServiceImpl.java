package com.code.jewelry_shop.service.impl;


import com.code.jewelry_shop.dto.response.ResponseOrderDetail;
import com.code.jewelry_shop.entity.*;
import com.code.jewelry_shop.repository.OrderDetailRepository;
import com.code.jewelry_shop.service.IOrderDetailService;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderDetailServiceImpl implements IOrderDetailService {
    private final Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    OrderDetailRepository orderDetailRepository;

    @Override
    @Transactional
    public List<ResponseOrderDetail> createOrderDetail(Cart cart, Order order) {
        List<CartDetail> cartDetails = cart.getCartDetails();
//        List<OrderDetail> orderDetails = new ArrayList<>();
        List<ResponseOrderDetail> lResponseOrderDetails = new ArrayList<>();
        for (CartDetail cartDetail : cartDetails) {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setProduct(cartDetail.getProduct());
            orderDetail.setQuantity(cartDetail.getQuantity());
            orderDetail.setUnitPrice(cartDetail.getUnitPrice());
            orderDetail.setSubTotalPrice(cartDetail.getPrice());
            orderDetail.setOrder(order);
            order.getOrderDetails().add(orderDetail);
            orderDetailRepository.save(orderDetail);
            ResponseOrderDetail lResponseOrderDetail = new ResponseOrderDetail(orderDetail);
            lResponseOrderDetails.add(lResponseOrderDetail);
        }
        log.debug("Created order");
        return lResponseOrderDetails;
    }
    @Override
    public ResponseOrderDetail getByOrdersId(Long orderId) {
        OrderDetail orderDetail = orderDetailRepository.findByOrder_Id(orderId);
        return modelMapper.map(orderDetail, ResponseOrderDetail.class);
    }

    @Override
    public List<ResponseOrderDetail> findAll() {
        List<OrderDetail> orderDetails = orderDetailRepository.findAll();
        return orderDetails.stream()
                .map(orderDetail -> modelMapper.map(orderDetail, ResponseOrderDetail.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<OrderDetail> getOrderDetailsByOrderId(Long orderId) {
        return orderDetailRepository.findAllByOrderId_Id(orderId);
    }

}
