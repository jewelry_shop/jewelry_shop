package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.dto.request.forCreate.CRequestParameterDetail;
import com.code.jewelry_shop.dto.request.forUpdate.URequestParameterDetail;
import com.code.jewelry_shop.entity.ParameterDetail;
import com.code.jewelry_shop.entity.Product;
import com.code.jewelry_shop.repository.ParameterDetailRepository;
import com.code.jewelry_shop.repository.ProductRepository;
import com.code.jewelry_shop.service.IParameterDetailService;
import com.code.jewelry_shop.utils.exception.ApiException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Service
public class ParameterDetailServiceImpl implements IParameterDetailService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private ParameterDetailRepository parameterDetailRepository;


    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    @Transactional
    public CRequestParameterDetail create(Long productId, CRequestParameterDetail cRequestParameterDetail) {

        productRepository.findById(productId).orElseThrow(() ->new ApiException("Product not found with id: {}" + productId, HttpStatus.NOT_FOUND));
        ParameterDetail parameterDetail = modelMapper.map(cRequestParameterDetail, ParameterDetail.class);
        cRequestParameterDetail.setProductId(productId);

        parameterDetailRepository.save(parameterDetail);
        log.debug("Created Parameter: {}", cRequestParameterDetail);
        return cRequestParameterDetail;
    }


    @Override
    @Transactional
    public boolean updateParameterDetailByProductId(Long productId, URequestParameterDetail uRequestParameterDetail) {
        Optional<Product> product = productRepository.findById(productId);
        if(product.isPresent()) {
            ParameterDetail parameterDetail = parameterDetailRepository.findByProductId(productId);

            modelMapper.map(uRequestParameterDetail, parameterDetail);
            parameterDetailRepository.save(parameterDetail);
            log.debug("Updated Parameter: {}", uRequestParameterDetail);
            return true;
        }
        log.error("An error occurred when updating Parameter");
        return false;
    }



}
