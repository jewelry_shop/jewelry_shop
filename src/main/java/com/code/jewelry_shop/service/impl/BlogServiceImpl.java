package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.dto.request.forCreate.CRequestBlog;
import com.code.jewelry_shop.dto.request.forCreate.URequestBlog;
import com.code.jewelry_shop.dto.response.forBlog.DResponseBlog;
import com.code.jewelry_shop.dto.response.forBlog.LResponseBlog;
import com.code.jewelry_shop.dto.response.forBlog.LResponseBlogLink;
import com.code.jewelry_shop.entity.Blog;
import com.code.jewelry_shop.entity.BlogImage;
import com.code.jewelry_shop.repository.BlogImageRepository;
import com.code.jewelry_shop.repository.BlogRepository;
import com.code.jewelry_shop.repository.BlogTypeRepository;
import com.code.jewelry_shop.service.IBlogService;
import com.code.jewelry_shop.utils.exception.ApiException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class BlogServiceImpl implements IBlogService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private BlogRepository blogRepository;

    @Autowired
    private BlogImageRepository blogImageRepository;

    @Autowired
    private BlogTypeRepository blogTypeRepository;

    @Autowired
    private FirebaseImageService firebaseImageService;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    @Transactional
    public DResponseBlog createBlog(CRequestBlog cRequestBlog){
        try {
            Blog savedBlog = modelMapper.map(cRequestBlog, Blog.class);
            savedBlog = blogRepository.save(savedBlog);
            saveBlogImages(cRequestBlog.getFiles(), savedBlog);
            DResponseBlog dResponseBlog = new DResponseBlog();
            dResponseBlog.setBlog(savedBlog);
            dResponseBlog.setBannerImage(getBlogImageLinks(savedBlog));

            log.debug("Created Blog: {}", dResponseBlog);
            return dResponseBlog;
        }catch(Exception e) {
            log.error("Unexpected error occurred while creating Blog", e);
            throw new ApiException("Failed to create Blog");
        }
    }

    @Override
    @Transactional
    public DResponseBlog updateBlog(URequestBlog urequestBlog){
        try{
            Blog existingBlog = blogRepository.findById(urequestBlog.getId()).orElseThrow(() -> new ApiException("Blog not found with id:{}"+ urequestBlog.getId(), HttpStatus.NOT_FOUND));
            modelMapper.map(urequestBlog, existingBlog);
            if (urequestBlog.getFiles() != null && urequestBlog.getFiles().length > 0 && !areAllFilesEmpty(urequestBlog.getFiles())) {  // Check if new non-empty images were uploaded
                deleteOldBlogImages(existingBlog.getId());          // Delete old product images from Firebase Storage
                saveBlogImages(urequestBlog.getFiles(), existingBlog);           // Save the new product images
            }
            Blog savedBlog = blogRepository.save(existingBlog);
            DResponseBlog responseBlog = modelMapper.map(savedBlog, DResponseBlog.class);
            responseBlog.setBannerImage(getBlogImageLinks(savedBlog));
            log.debug("Updated Blog: {}", urequestBlog);
            return responseBlog;
        }catch (Exception e) {
            log.error("Unexpected error occurred while updating Blog", e);
            throw new ApiException("Failed to update Blog");
        }
    }

    @Override
    @Transactional
    public boolean deleteBlog(Long id){
        Optional<Blog> blog = blogRepository.findById(id);
        if(blog.isPresent()){
            List<BlogImage> blogImages = blogImageRepository.findByBlogId(id);
            for(BlogImage blogImage : blogImages){
                String imageUrl = blogImage.getLink();
                String fileName = extractFilenameFromUrl(imageUrl);
                try {
                    log.info("Deleted Blog");
                    firebaseImageService.delete(fileName);
                } catch (IOException e) {
                    log.error("Unexpected error occurred while deleting Blog", e);
                    throw new ApiException("Failed to delete Blog");
                }
            }
            blogImageRepository.deleteByBlogId(id);
            blogRepository.deleteBlogById(id);
            return true;
        }
        return false;

    }

    @Override
    public DResponseBlog getById(Long id){
        Blog blog = blogRepository.findById(id).orElseThrow(() -> new ApiException("Blog not found with id:{}"+ id, HttpStatus.NOT_FOUND));
        List<String> image = blogImageRepository.findByBlogId(id)
                .stream()
                .map(BlogImage::getLink)
                .toList();

        DResponseBlog dResponseBlog = new DResponseBlog();
        dResponseBlog.setBlog(blog);
        dResponseBlog.setBannerImage(new HashSet<>(image));
        return dResponseBlog;
    }

    @Override
    public List<Blog> getAllBlogs() {
        return blogRepository.findAll();
    }

    @Override
    public List<LResponseBlogLink> getBlogByLink(String linkImage) throws IOException {
        List<LResponseBlogLink> lResponseBlogLinks =  new ArrayList<>();
        List<Blog> blogs = blogRepository.findAll();
        for(Blog blog : blogs){
            List<String> imageList = firebaseImageService.getImageUrlsFromDescription(blog.getDescription());
            if(!imageList.isEmpty()){
                for(String image : imageList){
                    if(linkImage.contains(image)){
                        LResponseBlogLink lResponseBlogLink = new LResponseBlogLink();
                        lResponseBlogLink.setLinkContent(blog.getLinkContent());
                        lResponseBlogLink.setLink(blogTypeRepository.findById(blog.getBlogType().getId()).orElseThrow(() -> new ApiException("Blog Type not found", HttpStatus.NOT_FOUND)).getLink());
                        lResponseBlogLink.setLinkImage(image);
                        lResponseBlogLinks.add(lResponseBlogLink);
                        break;

                    }
                }
            }
        }
        return lResponseBlogLinks;
    }

    @Override
    public List<LResponseBlog> getBlogByTitle(String title){
        List<Blog> blogsList = blogRepository.findByTitleContainingIgnoreCase(title);
        List<LResponseBlog> lResponseBlogs = new ArrayList<>();
        for(Blog blog : blogsList){
            LResponseBlog lResponseBlog = new LResponseBlog();
            modelMapper.map(lResponseBlog, blog);
            lResponseBlog.setBannerImg(getFirstImageLink(blog));
            lResponseBlogs.add(lResponseBlog);
        }
        return lResponseBlogs;
    }

    @Override
    public List<LResponseBlog> getAllBlogByType(String link){
        return blogRepository.findAllByBlogType(blogTypeRepository.findFirstByLink(link))
                .stream()
                .map(blog -> modelMapper.map(blog, LResponseBlog.class))
                .toList();
    }


    @Override
    public Map<String, List<String>> reviewElementsBlog() {
        try {
            List<String> imageUrlsAllDescription = getAllImageUrlsInDescriptionBlog();
            List<String> imageUrlsAllFirestore = firebaseImageService.getAllImageUrlsInFirestore();
            List<String> imageUrlsWithMediaLink = new ArrayList<>();
            for (String imageUrl : imageUrlsAllDescription) {
                String mediaLink = firebaseImageService.getMediaLinkFromImageUrl(imageUrl);
                if (mediaLink != null) {
                    imageUrlsWithMediaLink.add(mediaLink);
                }
            }

            List<String> uniqueUrlsInFirestore = new ArrayList<>(imageUrlsAllFirestore);
            uniqueUrlsInFirestore.removeAll(imageUrlsWithMediaLink);

            List<String> uniqueUrlsInDescription = new ArrayList<>(imageUrlsWithMediaLink);
            uniqueUrlsInDescription.removeAll(imageUrlsAllFirestore);
            List<String> signedUrlsInFirestore = firebaseImageService.signUrls(uniqueUrlsInFirestore);
            Map<String, List<String>> result = new HashMap<>();
            result.put("UniqueUrlsInFirestore", signedUrlsInFirestore);
            result.put("UniqueUrlsInDescription", uniqueUrlsInDescription);
            return result;
        } catch (Exception e) {
            log.error("An error when reviewing Blog", e);
            return null;
        }
    }


    private String getFirstImageLink(Blog blog) {
        List<BlogImage> blogImages = blogImageRepository.findByBlogId(blog.getId());
        if (!blogImages.isEmpty()) {
            return blogImages.get(0).getLink();
        }
        return null;
    }

    public List<String> getAllImageUrlsInDescriptionBlog() throws IOException {
        List<Blog> blogs = blogRepository.findAll();
        List<String> imageUrls = new ArrayList<>();
        for (Blog blog : blogs) {
            List<String> imageUrlList = firebaseImageService.getImageUrlsFromDescription(blog.getDescription());
            imageUrls.addAll(imageUrlList);
        }
        return imageUrls;
    }

    @Transactional
    protected void saveBlogImages(MultipartFile[] imageFiles, Blog savedBlog) {
        for (MultipartFile imageFile : imageFiles) {
            if (!imageFile.isEmpty()) {
                try {
                    String imageUrl = firebaseImageService.save(imageFile);
                    createBlogImageEntity(savedBlog, imageUrl);
                } catch (IOException e) {
                    log.error("Unexpected error occurred when saving Blog images");
                }
            }
        }
    }

    @Transactional
    protected void createBlogImageEntity(Blog savedBlog, String imageUrl){
        BlogImage blogImage = new BlogImage();
        blogImage.setLink(imageUrl);
        blogImage.setBlog(savedBlog);
        blogImageRepository.save(blogImage);
    }

    private Set<String> getBlogImageLinks(Blog savedBlog) {
        Set<String> imageLinks = new HashSet<>();
        List<BlogImage> blogId = blogImageRepository.findByBlogId(savedBlog.getId());
        for (BlogImage blogImage : blogId) {
            imageLinks.add(blogImage.getLink());
        }
        return imageLinks;
    }

    private boolean areAllFilesEmpty(MultipartFile[] files) {
        for (MultipartFile file : files) {
            if (!file.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    @Transactional
    protected void deleteOldBlogImages(Long blogId) {
        List<BlogImage> blogImages = blogImageRepository.findByBlogId(blogId);
        for (BlogImage blogImage : blogImages) {
            String imageUrl = blogImage.getLink();
            String fileName = extractFilenameFromUrl(imageUrl);
            try {
                firebaseImageService.delete(fileName);
            } catch (IOException e) {
                log.error("Unexpected error occurred when deleting old Blog images", e);
            }
        }
        blogImageRepository.deleteByBlogId(blogId);
    }

    private String extractFilenameFromUrl(String imageUrl) {
        int startIndex = imageUrl.lastIndexOf("/") + 1;
        int endIndex = imageUrl.indexOf("?");
        return imageUrl.substring(startIndex, endIndex);
    }

}
