package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.dto.request.forCreate.CRequestProduct;
import com.code.jewelry_shop.dto.request.forUpdate.URequestProduct;
import com.code.jewelry_shop.dto.response.forDetail.DResponseProduct;
import com.code.jewelry_shop.dto.response.forList.LResponseProduct;
import com.code.jewelry_shop.entity.*;
import com.code.jewelry_shop.repository.*;
import com.code.jewelry_shop.service.IProductService;
import com.code.jewelry_shop.utils.exception.ApiException;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Service
public class ProductServiceImpl implements IProductService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ParameterDetailRepository parameterDetailRepository;

    @Autowired
    private ProductImageRepository productImageRepository;

    @Autowired
    private ManufacturerRepository manufacturerRepository;

    @Autowired
    private CategoryCRepository categoryCRepository;

    @Autowired
    private FirebaseImageService firebaseImageService;

    @Autowired
    private ProductColorRepository productColorRepository;

    @Autowired
    private SpecialDiscountRepository specialDiscountRepository;


    @Override
    @Transactional
    public DResponseProduct addProduct(CRequestProduct cRequestProduct) {
        try {
            Product savedProduct = mapCProductToEntity(cRequestProduct);
            savedProduct = productRepository.save(savedProduct);
            saveProductImages(cRequestProduct.getFiles(), savedProduct);
            DResponseProduct responseDTO = new DResponseProduct();
            responseDTO.setProduct(savedProduct);
            responseDTO.setImageLinks(getProductImageLinks(savedProduct));

            return responseDTO;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ApiException("Failed to add product");
        }
    }

    private Product mapCProductToEntity(CRequestProduct cRequestProduct) {
        Product product = modelMapper.map(cRequestProduct, Product.class);

        if (cRequestProduct.getSpecialDiscounts() != null) {
            for (SpecialDiscount specialDiscountId : cRequestProduct.getSpecialDiscounts()) {
                SpecialDiscount specialDiscount = specialDiscountRepository.findById(specialDiscountId.getId())
                        .orElseThrow(() -> new ApiException("Special Discount not found with id: " + specialDiscountId));
                BigDecimal discountPercentage = specialDiscount.getDiscountPercentage();
                BigDecimal productPrice = product.getPrice();
                BigDecimal discountAmount = productPrice.multiply(discountPercentage.divide(BigDecimal.valueOf(100), RoundingMode.HALF_UP));
                product.setDiscountedPrice(discountAmount);
            }
        }

        if (cRequestProduct.getManufacturer() != null) {
            Manufacturer manufacturer = manufacturerRepository.findById(cRequestProduct.getManufacturer().getId()).orElse(null);
            product.setManufacturer(manufacturer);
        }

        if (cRequestProduct.getCategoryC() != null) {
            CategoryC categoryC = categoryCRepository.findById(cRequestProduct.getCategoryC().getId()).orElse(null);
            product.setCategoryC(categoryC);
        }

        if (cRequestProduct.getParameterDetail() != null){
            ParameterDetail parameterDetail = parameterDetailRepository.findById(cRequestProduct.getParameterDetail().getId()).orElse(null);
            product.setParameterDetail(parameterDetail);
        }

        return product;
    }


    private Set<String> getProductImageLinks(Product savedProduct) {
        Set<String> imageLinks = new HashSet<>();
        List<ProductImage> productImages = productImageRepository.findByProductId(savedProduct.getId());
        for (ProductImage productImage : productImages) {
            imageLinks.add(productImage.getLink());
        }
        return imageLinks;
    }

    private String getFirstImageLink(Product product) {
        List<ProductImage> productImages = productImageRepository.findByProductId(product.getId());
        if (!productImages.isEmpty()) {
            return productImages.get(0).getLink();
        }
        return null;
    }


    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public List<LResponseProduct> searchProducts(String keyword) {
        List<Product> matchingProducts = productRepository.findByProductNameContainingIgnoreCase(keyword);
        List<LResponseProduct> lProductDTOs = new ArrayList<>();
        for (Product product : matchingProducts) {
            LResponseProduct lProductDTO = new LResponseProduct();
            lProductDTO.setId(product.getId());
            lProductDTO.setProductName(product.getProductName());
            lProductDTO.setPrice(product.getPrice());
            lProductDTO.setImageLinks(getFirstImageLink(product));
            lProductDTOs.add(lProductDTO);
        }
        return lProductDTOs;
    }

    @Override
    public DResponseProduct getProductById(Long productId) {
        Product product = productRepository.findById(productId).orElseThrow(() -> new ApiException("Product not found with id:{}"+ productId, HttpStatus.NOT_FOUND));
        ParameterDetail parameterDetail = parameterDetailRepository.findByProductId(productId);
        List<String> imageLinks = productImageRepository.findByProductId(productId)
                .stream()
                .map(ProductImage::getLink)
                .toList();

        DResponseProduct dResponseProduct = new DResponseProduct();
        dResponseProduct.setProduct(product);
        dResponseProduct.setParameterDetail(parameterDetail);
        dResponseProduct.setImageLinks(new HashSet<>(imageLinks));
        return dResponseProduct;
    }

    @Override
    @Transactional
    public URequestProduct updateProductInfo(URequestProduct uRequestProduct) {
        try {
            Product existingProduct = productRepository.findById(uRequestProduct.getId())
                    .orElseThrow(() -> new ApiException("Product not found"));
            updateProductFields(uRequestProduct, existingProduct);        // Update the product fields
            if (uRequestProduct.getFiles() != null && uRequestProduct.getFiles().length > 0 && !areAllFilesEmpty(uRequestProduct.getFiles())) {  // Check if new non-empty images were uploaded
                deleteOldProductImages(existingProduct.getId());          // Delete old product images from Firebase Storage
                saveProductImages(uRequestProduct.getFiles(), existingProduct);           // Save the new product images
            }
            Product savedProduct = productRepository.save(existingProduct); // Save the updated product
            URequestProduct responseDTO = modelMapper.map(savedProduct, URequestProduct.class); // Create the response DTO
            responseDTO.setImageLinks(getProductImageLinks(savedProduct));

            return responseDTO;
        } catch (NoSuchElementException e) {
            throw new ApiException("Product not found");
        }
    }

    private void updateProductFields(URequestProduct uRequestProduct, Product existingProduct) {
        existingProduct.setProductName(uRequestProduct.getProductName());
        existingProduct.setPrice(uRequestProduct.getPrice());
        existingProduct.setStock(uRequestProduct.getStock());

        if (uRequestProduct.getSpecialDiscounts() != null) {
            for (SpecialDiscount specialDiscountId : uRequestProduct.getSpecialDiscounts()) {
                SpecialDiscount specialDiscount = specialDiscountRepository.findById(specialDiscountId.getId())
                        .orElseThrow(() -> new ApiException("Special Discount not found with id: " + specialDiscountId));
                BigDecimal discountPercentage = specialDiscount.getDiscountPercentage();
                BigDecimal productPrice = existingProduct.getPrice();
                BigDecimal discountAmount = productPrice.multiply(discountPercentage.divide(BigDecimal.valueOf(100), RoundingMode.HALF_UP));
                existingProduct.setDiscountedPrice(discountAmount);
            }
        }

        if (uRequestProduct.getCategoryC() != null) {
            CategoryC categoryC = categoryCRepository.findById(uRequestProduct.getCategoryC().getId())
                    .orElseThrow(() -> new ApiException("CategoryC not found", HttpStatus.NOT_FOUND));
            existingProduct.setCategoryC(categoryC);
        }

        // Update the manufacturer if it is not null
        if (uRequestProduct.getManufacturer() != null) {
            Manufacturer manufacturer = manufacturerRepository.findById(uRequestProduct.getManufacturer().getId())
                    .orElseThrow(() -> new ApiException("Manufacturer not found", HttpStatus.NOT_FOUND));
            existingProduct.setManufacturer(manufacturer);
        }

        if(uRequestProduct.getParameterDetail() != null){
            ParameterDetail parameterDetail = parameterDetailRepository.findById(uRequestProduct.getParameterDetail().getId())
                    .orElseThrow(() -> new ApiException("ParameterDetail not found", HttpStatus.NOT_FOUND));
        }
    }

    private boolean areAllFilesEmpty(MultipartFile[] files) {
        for (MultipartFile file : files) {
            if (!file.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    @Transactional
    protected void deleteOldProductImages(Long productId) {
        List<ProductImage> imageProducts = productImageRepository.findByProductId(productId);
        for (ProductImage imageProduct : imageProducts) {
            String imageUrl = imageProduct.getLink();
            String fileName = extractFilenameFromUrl(imageUrl);
            try {
                firebaseImageService.delete(fileName);
            } catch (IOException e) {
                e.printStackTrace();
                // Handle the image deletion error
            }
        }
        productImageRepository.deleteByProductId(productId);
    }

    @Transactional
    protected void saveProductImages(MultipartFile[] imageFiles, Product savedProduct) {
        for (MultipartFile imageFile : imageFiles) {
            if (!imageFile.isEmpty()) {
                try {
                    String imageUrl = firebaseImageService.save(imageFile);
                    createProductImageEntity(savedProduct, imageUrl);
                } catch (IOException e) {
                    e.printStackTrace();
                    // Handle the file saving error
                }
            }
        }
    }

    private void createProductImageEntity(Product savedProduct, String imageUrl) {
        ProductImage productImage = new ProductImage();
        productImage.setLink(imageUrl);
        productImage.setProduct(savedProduct);
        productImageRepository.save(productImage);
    }


    @Override
    @Transactional
    public boolean deleteProduct(Long productId) {
        Optional<Product> optionalProduct = productRepository.findById(productId);
        if (optionalProduct.isPresent()) {
            List<ProductImage> imageProducts = productImageRepository.findByProductId(productId);
            for (ProductImage imageProduct : imageProducts) {
                String imageUrl = imageProduct.getLink();
                String fileName = extractFilenameFromUrl(imageUrl);
                try {
                    firebaseImageService.delete(fileName);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            productColorRepository.deleteByProductId(productId);
            productImageRepository.deleteByProductId(productId);  // Delete product images from the database
            parameterDetailRepository.deleteByProductId(productId);  // Delete specification details
            productRepository.deleteById(productId);         // Delete the product

            // Delete related Rate entities

            return true;
        }
        return false;
    }

    private String extractFilenameFromUrl(String imageUrl) {
        int startIndex = imageUrl.lastIndexOf("/") + 1;
        int endIndex = imageUrl.indexOf("?");
        return imageUrl.substring(startIndex, endIndex);
    }

    @Override
    public Page<LResponseProduct> getAllProductDTOs(int page, Sort.Direction direction) {
        Pageable pageable = PageRequest.of(page, 10,Sort.by(direction,"id"));
        Page<Product> products = productRepository.findAll(pageable);

        List<LResponseProduct> lProductDTOs = new ArrayList<>();
        for (Product product : products) {
            LResponseProduct lProductDTO = new LResponseProduct();
            lProductDTO.setId(product.getId());
            lProductDTO.setProductName(product.getProductName());
            lProductDTO.setPrice(product.getPrice());
            lProductDTO.setImageLinks(getFirstImageLink(product));
            lProductDTOs.add(lProductDTO);
        }

        return new PageImpl<>(lProductDTOs, pageable, products.getTotalElements());
    }
    @Override
    public Page<LResponseProduct> Sorting(int page, Sort.Direction direction) {
        Pageable pageable = PageRequest.of(page, 10,Sort.by(direction,"price"));
        Page<Product> products = productRepository.findAll(pageable);

        List<LResponseProduct> lProductDTOs = new ArrayList<>();
        for (Product product : products) {
            LResponseProduct lProductDTO = new LResponseProduct();
            lProductDTO.setId(product.getId());
            lProductDTO.setProductName(product.getProductName());
            lProductDTO.setPrice(product.getPrice());
            lProductDTO.setImageLinks(getFirstImageLink(product));
            lProductDTOs.add(lProductDTO);
        }

        return new PageImpl<>(lProductDTOs, pageable, products.getTotalElements());
    }


    @Override
    public Page<Product> getAllProducts(int page) {
        Pageable pageable = PageRequest.of(page, 10);
        return productRepository.findAll(pageable);
    }

    @Override
    public List<LResponseProduct> getAllProductDiscountByCategory(CategoryC categoryC){
        List<Product> products = productRepository.findByCategoryC(categoryC);
        return mapProductsToLResponseProduct(products);
    }

    @Override
    public List<LResponseProduct> getAllProductDiscountByManufacture(Manufacturer manufacturerId){
        List<Product> products = productRepository.findByManufacturer(manufacturerId);
        return mapProductsToLResponseProduct(products);
    }



    @Override
    public List<LResponseProduct> getAllProductDiscountByDiscountPercentage(BigDecimal discountPercentage) {
        List<Product> products = productRepository.findAll();
        List<LResponseProduct> responseProducts = new ArrayList<>();

        for (Product product : products) {
            boolean hasDiscount = false;
            for (SpecialDiscount specialDiscount : product.getSpecialDiscounts()) {
                if (specialDiscount.getDiscountPercentage().compareTo(discountPercentage) == 0) {
                    hasDiscount = true;
                    break;
                }
            }
            if (hasDiscount) {
                LResponseProduct responseProduct = new LResponseProduct();
                responseProduct.setId(product.getId());
                responseProduct.setProductName(product.getProductName());
                responseProduct.setPrice(product.getPrice());
                responseProduct.setDiscountedPrice(product.getDiscountedPrice());
                responseProducts.add(responseProduct);
            }
        }

        return responseProducts;
    }

    private List<LResponseProduct> mapProductsToLResponseProduct(List<Product> products){
        List<LResponseProduct> responseProducts = new ArrayList<>();
        for (Product product : products) {
            if(product.getDiscountedPrice() != null){
                LResponseProduct responseProduct = new LResponseProduct();
                responseProduct.setId(product.getId());
                responseProduct.setProductName(product.getProductName());
                responseProduct.setPrice(product.getPrice());
                responseProduct.setDiscountedPrice(product.getDiscountedPrice());
                responseProducts.add(responseProduct);
            }
        }
        return responseProducts;
    }
}
