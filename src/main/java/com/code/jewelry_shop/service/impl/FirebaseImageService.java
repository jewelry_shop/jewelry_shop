package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.service.IImageService;
import com.google.api.core.ApiFuture;
import com.google.api.gax.paging.Page;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.storage.*;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.StorageClient;
import lombok.Data;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
public class FirebaseImageService implements IImageService {

    private Firestore firestore;
    private String bucketName;

    @Autowired
    @Qualifier("firebaseImageServiceProperties")
    private Properties properties;
    @EventListener
    public void init(ApplicationReadyEvent event) {
        try {
            ClassPathResource serviceAccount = new ClassPathResource("firebase.json");
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount.getInputStream()))
                    .setStorageBucket(properties.getBucketName())
                    .build();
            FirebaseApp.initializeApp(options);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public String getImageUrl(String name) {
        return String.format(properties.getImageUrl(), name);
    }

    @Override
    public String save(MultipartFile file) throws IOException {
        Bucket bucket = StorageClient.getInstance().bucket();
        String name = generateFileName(file.getOriginalFilename());
        Blob blob = bucket.create(name, file.getInputStream(), file.getContentType());

        // Get the public URL of the saved file
        String imageUrl = blob.signUrl(365, TimeUnit.DAYS).toString();

        return imageUrl;
    }


    @Override
    public String save(BufferedImage bufferedImage, String originalFileName) throws IOException {
        byte[] bytes = getByteArrays(bufferedImage, getExtension(originalFileName));
        Bucket bucket = StorageClient.getInstance().bucket();
        String name = generateFileName(originalFileName);
        bucket.create(name, bytes);
        return name;
    }

    @Override
    public void delete(String name) throws IOException {
        if (StringUtils.isEmpty(name)) {
            throw new IOException("Invalid file name");
        }
        String bucketName = "jewelry-shop-3f9ef.appspot.com";
        Storage storage = StorageOptions.newBuilder().setProjectId(bucketName).build().getService();
        BlobId blobId = BlobId.of(bucketName, name);
        boolean deleted = storage.delete(blobId);
        if (!deleted) {
            throw new IOException("File not found");
        }
    }

    public List<String> getImageUrlsFromDescription(String description) throws IOException {
        List<String> imageUrls = new ArrayList<>();
        Document doc = Jsoup.parse(description);
        Elements imgElements = doc.select("img");
        for (Element img : imgElements) {
            String imageUrl = img.attr("src");
            imageUrls.add(imageUrl);
        }
        return imageUrls;
    }

    public List<String> getAllImageUrlsInFirestore() throws ExecutionException, InterruptedException {
        List<String> imageUrls = new ArrayList<>();
        Bucket bucket = StorageClient.getInstance().bucket();
        System.out.println(bucket);
        Page<Blob> blobs = bucket.list();
        for (Blob blob : blobs.iterateAll()) {
            String imageUrl = blob.getMediaLink();
            System.out.println("Retrieved image URL: " + imageUrl);
            imageUrls.add(imageUrl);
        }
        System.out.println("Total image URLs retrieved: " + imageUrls.size());
        System.out.println(imageUrls);
        return imageUrls;
    }

    public List<String> signUrls(List<String> imageUrls) {
        List<String> signedUrls = new ArrayList<>();
        Storage storage = StorageOptions.getDefaultInstance().getService();

        for (String imageUrl : imageUrls) {
            String objectName = getImageObjectName(imageUrl);
            BlobId blobId = BlobId.of(bucketName, objectName);
            Blob blob = storage.get(blobId);

            if (blob != null && blob.exists()) {
                // SignUrl with time is 7 day
                String signedUrl = blob.signUrl(7, TimeUnit.DAYS).toString();
                signedUrls.add(signedUrl);
            }
        }

        return signedUrls;
    }

    public List<BlobId> getListImageInHtml(String text) {
        List<BlobId> blobIds = new ArrayList<>();
        Document doc = Jsoup.parse(text);
        Elements imgElements = doc.select("img");

        for (Element img : imgElements) {
            String imageUrl = img.attr("src");
            String objectName = getImageObjectName(imageUrl);
            if (objectName != null) {
                BlobId blobId = BlobId.of(bucketName, objectName);
                blobIds.add(blobId);
            } else {
                System.out.println("Invalid image URL: " + imageUrl);
            }
        }

        return blobIds;
    }

    public String uploadFile(MultipartFile multipartFile) throws IOException {
        Bucket bucket = StorageClient.getInstance().bucket();
        String name = generateFileName(multipartFile.getOriginalFilename());
        Blob blob = bucket.create(name, multipartFile.getInputStream(), multipartFile.getContentType());

        // Get the public URL of the saved file
        String imageUrl = blob.signUrl(365, TimeUnit.DAYS).toString();

        return imageUrl;
    }

    public String deleteFile(String imageUrl) throws IOException {

        Storage storage = StorageClient.getInstance().bucket().getStorage();

        String objectName = getImageObjectName(imageUrl);
        if (objectName != null) {
            BlobId blobId = BlobId.of(bucketName, objectName);
            Blob blob = storage.get(blobId);

            if (blob != null && blob.exists()) {
                boolean deleted = storage.delete(blobId);
                if (deleted) {
                    return "Image deleted successfully.";
                } else {
                    return "Failed to delete the image.";
                }
            } else {
                return "Image does not exist on Firebase Storage.";
            }
        } else {
            return "Invalid image URL.";
        }
    }

    public List<String> deleteFiles(List<BlobId> blobIds) throws IOException {
        Storage storage = StorageClient.getInstance().bucket().getStorage();

        List<String> failedImageUrls = new ArrayList<>();

        if (!blobIds.isEmpty()) {
            List<Boolean> deletedResults = storage.delete(blobIds);

            // Iterate through the deletedResults list and check for failed deletions
            for (int i = 0; i < deletedResults.size(); i++) {
                if (!deletedResults.get(i)) {
                    // Get the corresponding BlobId and extract the image URL
                    BlobId blobId = blobIds.get(i);
                    String imageUrl = "gs://" + blobId.getBucket() + "/" + blobId.getName();
                    failedImageUrls.add(imageUrl);
                }
            }

            if (failedImageUrls.isEmpty()) {
                return Collections.singletonList("All images deleted successfully.");
            } else {
                return failedImageUrls;
            }
        } else {
            return Collections.emptyList();
        }
    }

    public String getMediaLinkFromImageUrl(String imageUrl) {
        String objectName = getImageObjectName(imageUrl);
        Bucket bucket = StorageClient.getInstance().bucket();
        Blob blob = bucket.get(objectName);

        if (blob != null && blob.exists()) {
            return blob.getMediaLink();
        }

        // Trả về imageUrl nếu không thể lấy được mediaLink
        return imageUrl;
    }


    public String getImageObjectName(String imageUrl) {
        if (imageUrl != null) {
            int startIndex = imageUrl.lastIndexOf("/") + 1;
            int endIndex = imageUrl.indexOf("?");

            if (endIndex == -1) {
                endIndex = imageUrl.length();
            }

            return imageUrl.substring(startIndex, endIndex);
        }
        return null;
    }


    public String generateFileName(String multiPart) {
        return new Date().getTime() + ".jpg";
    }

    public boolean imageExistsInDatabase(String imageUrl) throws ExecutionException, InterruptedException {
        DocumentReference docRef = firestore.collection("images").document(getImageObjectName(imageUrl));
        ApiFuture<DocumentSnapshot> future = docRef.get();
        DocumentSnapshot document = future.get();

        return document.exists();
    }



    @Data
    @Configuration
    @ConfigurationProperties(prefix = "firebase.json")
    public static class Properties {
        private String bucketName;
        private String imageUrl;
    }
}