package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.dto.request.forCreate.CRequestOrder;
import com.code.jewelry_shop.dto.response.ResponseCart;
import com.code.jewelry_shop.dto.response.ResponseCartDetail;
import com.code.jewelry_shop.dto.response.ResponseOrderDetail;
import com.code.jewelry_shop.dto.response.forDetail.DCResponseCart;
import com.code.jewelry_shop.entity.*;
import com.code.jewelry_shop.repository.*;
import com.code.jewelry_shop.service.ICartService;
import com.code.jewelry_shop.service.IOrderDetailService;
import com.code.jewelry_shop.service.IOrderService;
import com.code.jewelry_shop.utils.exception.ApiException;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CartServiceImpl implements ICartService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private IOrderService orderService;

    @Autowired
    private IOrderDetailService orderDetailService;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private CartDetailRepository cartDetailRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private  ProductImageRepository productImageRepository;

    @Autowired
    private  ProductColorRepository productColorRepository;

    @Override
    public ResponseCart getCartById(Long userId) {
        Cart cart = cartRepository.findByUserId(userId)
                .orElseThrow(() -> new ApiException("Cart not found with userid: {}"+ userId, HttpStatus.NOT_FOUND));

        List<CartDetail> cartDetails = cartDetailRepository.findByCart(cart);

        BigDecimal total = cartDetails.stream()
                .map(cartDetail -> cartDetail.getUnitPrice().multiply(BigDecimal.valueOf(cartDetail.getQuantity())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        cart.setTotal(total);

        List<ResponseCartDetail> responseCartDetails = cartDetails.stream()
                .map(this::mapToCartDetail)
                .collect(Collectors.toList());

        ResponseCart responseCart = new ResponseCart();
        responseCart.setUserId(userId);
        responseCart.setCartDetailDTOList(responseCartDetails);

        return responseCart;
    }
//        List<CartDetail> cartDetails = cartDetailRepository.findByCartId(cart);
//        List<ResponseCartDetail> responseCartDetails = new ArrayList<>();
//        BigDecimal total = BigDecimal.ZERO;
//        for (CartDetail cartDetail : cartDetails) {
//            ResponseCartDetail responseCartDetail = mapToCartDetailDTO(cartDetail);
//            responseCartDetails.add(responseCartDetail);
//            total = total.add(cartDetail.getUnitPrice().multiply(BigDecimal.valueOf(cartDetail.getQuantity())));
//
//        }
//        cart.setTotal(total);

    @Override
    @Transactional
    public ResponseCart addProductToCart(Long userId, Long productId, Long colorId) {
        User user = userRepository.findById(userId)
                .orElseGet(() -> {
                    User newUser = new User();
                    newUser.setCreateAt(LocalDateTime.now());
                    return userRepository.save(newUser);
                });

        Cart cart = cartRepository.findByUserId(userId)
                .orElseGet(() -> {
                    Cart newCart = new Cart();
                    newCart.setUser(user);
                    return cartRepository.save(newCart);
                });

        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new ApiException("Product not found with ID: " + productId, HttpStatus.NOT_FOUND));

        List<CartDetail> cartDetails = cartDetailRepository.findByCart(cart);

        CartDetail existingCartDetail = null;
        for (CartDetail cartDetail : cartDetails) {
            if (cartDetail.getProduct().equals(product)) {
                existingCartDetail = cartDetail;
                break;
            }
        }

        if (existingCartDetail != null) {
            existingCartDetail.setQuantity(existingCartDetail.getQuantity() + 1);
            cartDetailRepository.save(existingCartDetail);
        } else {
            CartDetail cartDetail = new CartDetail();
            cartDetail.setProduct(product);
            cartDetail.setQuantity(1);
            BigDecimal price;
            if(cartDetail.getProduct().getDiscountedPrice() != null) {
                cartDetail.setUnitPrice(product.getDiscountedPrice());
                price = product.getDiscountedPrice().multiply(BigDecimal.valueOf(cartDetail.getQuantity()));
                cartDetail.setPrice(price);
            }
            else{
                cartDetail.setUnitPrice(product.getPrice());
                price = product.getPrice().multiply(BigDecimal.valueOf(cartDetail.getQuantity()));
                cartDetail.setPrice(price);
            }
            cartDetail.setCart(cart);
            ProductColor color = productColorRepository.findByProductIdAndColorId(productId,colorId);
            cartDetail.setProductColor(color.getColor().getId());
            cartDetailRepository.save(cartDetail);
        }

        ResponseCart responseCart = new ResponseCart();
        responseCart.setUserId(userId);
        responseCart.setCartDetailDTOList(getCartDetailsByCartId(cart.getId()));
        return responseCart;
    }

    @Override
    public List<ResponseCartDetail> getCartDetailsByCartId(Long userId) {

        Cart cart = cartRepository.findByUserId(userId).orElseThrow(() -> new ApiException("Cart not found with userId: {}"+  userId, HttpStatus.NOT_FOUND));
        return getResponseCartDetails(cart);
    }


    @Override
    @Transactional
    public boolean clearCart(Long userId) {

        Cart cart = cartRepository.findByUserId(userId).orElseThrow(() -> new ApiException("Cart not found with userid: {}"+ userId, HttpStatus.NOT_FOUND));
        List<CartDetail> cartDetails = cartDetailRepository.findByCart(cart);
        cartDetailRepository.deleteAll(cartDetails);
        cart.setTotal(BigDecimal.ZERO);
        cartRepository.save(cart);
        log.info("Cart successfully cleared for user with ID: {}", userId);
        return true;
    }



    @Override
    @Transactional
    public boolean removeCartDetail(Long cartDetailId) {
        CartDetail cartDetail = cartDetailRepository.findById(cartDetailId)
                .orElseThrow(() -> new ApiException("Cart detail not found with ID: " + cartDetailId, HttpStatus.NOT_FOUND));

            Cart cart = cartDetail.getCart();

            List<CartDetail> cartDetails = cartDetailRepository.findByCart(cart);

            cartDetails.removeIf(cd -> cd.getId().equals(cartDetailId));

            cartRepository.save(cart);

            cartDetailRepository.delete(cartDetail);
            log.info("Cart detail successfully cleared with ID: {}", cartDetailId);
            return true;
    }

    @Override
    public List<ResponseCartDetail> getAllCartDetails(Long userId) {
        Cart cart = cartRepository.findByUserId(userId).orElseThrow(() -> new ApiException("Cart not found with userid: {}"+ userId, HttpStatus.NOT_FOUND));
        return getResponseCartDetails(cart);
    }

    @Override
    public ResponseCart getCartByUserId(Long userId) {

        Cart cart = cartRepository.findByUserId(userId).orElseThrow(() -> new ApiException("Cart not found with userid"+ userId, HttpStatus.NOT_FOUND));

        return convertCartDetailsToDTO(cart);
    }

    @Override
    @Transactional
    public ResponseCartDetail updateCartDetail(Long cartDetailId, ResponseCartDetail cartDetail, int quantityChange) {
        CartDetail existingCartDetail = cartDetailRepository.findById(cartDetailId).orElse(null);
        if (existingCartDetail != null) {
            int newQuantity = existingCartDetail.getQuantity() + quantityChange;
            if (newQuantity >= 1 && newQuantity <= 3) {
                existingCartDetail.setQuantity(newQuantity);
                CartDetail updatedCartDetail = cartDetailRepository.save(existingCartDetail);
                return mapToCartDetail(updatedCartDetail);
            } else {
                log.error("Invalid quantity. Quantity should be between 1 and 3.");
                throw new ApiException("Invalid quantity. Quantity should be between 1 and 3.");
            }
        }
        log.error("Cart detail with ID {} doesn't exist", cartDetailId);
        return null;
    }

    @Override
    public DCResponseCart checkoutCart(Cart cart, User user) {

        if (cartDetailRepository.findByCart(cart).isEmpty()) {
            throw new ApiException("Cart is empty");
        }

        CRequestOrder cRequestOrder = new CRequestOrder();

        Order order = orderService.create(cRequestOrder, user);

        List<ResponseOrderDetail> orderDetail = orderDetailService.createOrderDetail(cart, order);

        cartRepository.delete(cart);

        DCResponseCart dcResponseCart = new DCResponseCart();
        dcResponseCart.setFullName(user.getFullName());
        dcResponseCart.setEmail(user.getEmail());
        dcResponseCart.setPhone(user.getPhone());
        dcResponseCart.setDateOfBirth(user.getDateOfBirth());
        dcResponseCart.setOrderDetail(orderDetail);
        dcResponseCart.setStatusOrder(order.getStatus());
        return dcResponseCart;
    }


    private ResponseCart convertCartDetailsToDTO(Cart cart) {
        List<CartDetail> cartDetails = cartDetailRepository.findByCart(cart);

        List<ResponseCartDetail> cartDetailDTOs = cartDetails.stream()
                .map(this::mapToCartDetail)
                .collect(Collectors.toList());

        ResponseCart cartDTO = new ResponseCart();
        cartDTO.setUserId(cart.getUser().getId());
        cartDTO.setCartDetailDTOList(cartDetailDTOs);

        return cartDTO;
    }

    private List<ResponseCartDetail> getResponseCartDetails(Cart cart) {
        List<CartDetail> cartDetails = cartDetailRepository.findByCart(cart);

        List<ResponseCartDetail> cartDetailDTOs = new ArrayList<>();
        for (CartDetail cartDetail : cartDetails) {
            ResponseCartDetail cartDetailDTO = mapToCartDetail(cartDetail);
            cartDetailDTOs.add(cartDetailDTO);
        }

        return cartDetailDTOs;
    }

    private ResponseCartDetail mapToCartDetail(CartDetail cartDetail) {
        ResponseCartDetail detail = new ResponseCartDetail();
        detail.setId(cartDetail.getId());
        detail.setProductName(cartDetail.getProduct().getProductName());
        detail.setQuantity(cartDetail.getQuantity());
        detail.setPrice(cartDetail.getPrice());
        detail.setUnitPrice(cartDetail.getUnitPrice());
        if(cartDetail.getProduct().getDiscountedPrice() != null){
            detail.setDiscountedPrice(cartDetail.getProduct().getDiscountedPrice());
        }
        String imageLink = getFirstImageLink(cartDetail.getProduct());
        detail.setProductImageLink(imageLink);
        detail.setProductColor(cartDetail.getProductColor());

        return detail;
    }



    private String getFirstImageLink(Product product) {
        List<ProductImage> productImages = productImageRepository.findByProductId(product.getId());
        if (!productImages.isEmpty()) {
            return productImages.get(0).getLink();
        }
        return null;
    }


}
