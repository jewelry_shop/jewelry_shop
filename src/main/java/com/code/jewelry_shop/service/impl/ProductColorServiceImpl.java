package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.dto.response.ResponseProductColor;
import com.code.jewelry_shop.entity.Color;
import com.code.jewelry_shop.entity.ProductColor;
import com.code.jewelry_shop.repository.ColorRepository;
import com.code.jewelry_shop.repository.ProductColorRepository;
import com.code.jewelry_shop.repository.ProductRepository;
import com.code.jewelry_shop.service.IProductColorService;
import com.code.jewelry_shop.entity.Product;
import com.code.jewelry_shop.utils.exception.ApiException;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductColorServiceImpl implements IProductColorService {

    @Autowired
    private ProductColorRepository productColorRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ColorRepository colorRepository;

    @Override
    public Map<String, Object> createProductColor(Long productId, List<Long> colors) {
        List<Map<String, Object>> createdProductColors = new ArrayList<>();
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new IllegalArgumentException("Product not found with ID: " + productId));

        if (colors != null) {
            processColors(product, colors, createdProductColors);
        }

        Map<String, Object> result = new HashMap<>();
        result.put("productId", productId);
        result.put("productColors", createdProductColors);

        return result;
    }


    private void processColors(Product product, List<Long> colors, List<Map<String, Object>> createdProductColors) {
        for (Long colorId : colors) {
            boolean colorExists = productColorRepository.existsByProductAndColorId(product, colorId);
            if (colorExists) {
                throw new ApiException("Color already exists for the product. Product ID: " + product.getId() + ", Color ID: " + colorId);
            }

            Color color = colorRepository.findById(colorId)
                    .orElseThrow(() -> new ApiException("Color not found with ID: " + colorId));

            ProductColor productColor = new ProductColor();
            productColor.setProduct(product);
            productColor.setColor(color);
            productColorRepository.save(productColor);

            Map<String, Object> colorMap = new HashMap<>();
            colorMap.put("id", colorId);
            colorMap.put("name", color.getColorName());

            createdProductColors.add(colorMap);
        }
    }

    @Override
    public ResponseProductColor getListDetailById(Long productId) {
        List<ProductColor> productColorList = productColorRepository.findAllByProductId(productId);

        return mapToProductColorDTOList(productColorList);
    }

    private ResponseProductColor mapToProductColorDTOList(List<ProductColor> productColorList) {
        ResponseProductColor productColorDTO = new ResponseProductColor();
        List<Color> colorList = new ArrayList<>();
        for (ProductColor productColor : productColorList) {
            productColorDTO.setProductId(productColor.getProduct().getId());
            Color color = productColor.getColor();
            colorList.add(color);
        }
        productColorDTO.setColors(colorList);
        return productColorDTO;
    }

    @Override
    @Transactional
    public ResponseProductColor updateProductColors(Long productId, List<Long> newColors) {
        Product product = getProductById(productId);
        List<ProductColor> existingProductColors = getExistingProductColors(productId);
        checkDuplicateColors(existingProductColors, newColors);
        List<ProductColor> updatedProductColors = createUpdatedProductColors(product, newColors);
        saveProductColors(updatedProductColors);

        List<Color> updatedColors = getUpdatedColors(updatedProductColors);

        return createProductColorDTO(productId, updatedColors);
    }


    private Product getProductById(Long productId) {
        return productRepository.findById(productId)
                .orElseThrow(() -> new ApiException("Product not found with ID: " + productId));
    }

    private List<ProductColor> getExistingProductColors(Long productId) {
        return productColorRepository.findAllByProductId(productId);
    }

    private void checkDuplicateColors(List<ProductColor> existingProductColors, List<Long> newColors) {
        Set<Long> existingColorIds = existingProductColors.stream()
                .map(productColor -> productColor.getColor().getId())
                .collect(Collectors.toSet());

        for (Long colorId : newColors) {
            if (existingColorIds.contains(colorId)) {
                throw new ApiException("Color with ID " + colorId + " already exists for the product");
            }
        }
    }

    private List<ProductColor> createUpdatedProductColors(Product product, List<Long> newColors) {
        return newColors.stream()
                .map(colorId -> createProductColor(product, colorId))
                .collect(Collectors.toList());
    }

    private ProductColor createProductColor(Product product, Long colorId) {
        Color color = getColorById(colorId);
        ProductColor productColor = new ProductColor();
        productColor.setProduct(product);
        productColor.setColor(color);
        return productColor;
    }

    private Color getColorById(Long colorId) {
        return colorRepository.findById(colorId)
                .orElseThrow(() -> new ApiException("Color not found with ID: " + colorId));
    }

    private void saveProductColors(List<ProductColor> productColors) {
        productColorRepository.saveAll(productColors);
    }

    private List<Color> getUpdatedColors(List<ProductColor> updatedProductColors) {
        return updatedProductColors.stream()
                .map(ProductColor::getColor)
                .collect(Collectors.toList());
    }

    private ResponseProductColor createProductColorDTO(Long productId, List<Color> updatedColors) {
        ResponseProductColor updatedProductColorDTO = new ResponseProductColor();
        updatedProductColorDTO.setProductId(productId);
        updatedProductColorDTO.setColors(updatedColors);
        return updatedProductColorDTO;
    }


}
