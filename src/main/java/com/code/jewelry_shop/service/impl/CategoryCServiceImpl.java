package com.code.jewelry_shop.service.impl;

import com.code.jewelry_shop.dto.response.forList.LResponseProduct;
import com.code.jewelry_shop.entity.*;
import com.code.jewelry_shop.repository.CategoryCRepository;
import com.code.jewelry_shop.repository.ProductImageRepository;
import com.code.jewelry_shop.repository.ProductRepository;
import com.code.jewelry_shop.service.ICategoryCService;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryCServiceImpl implements ICategoryCService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private CategoryCRepository categoryCRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductImageRepository productImageRepository;

    @Autowired
    private ModelMapper modelMapper;


    @Override
    @Transactional
    public CategoryC createCategoryC(CategoryC categoryC) {
        log.debug("Created CategoryC: {}", categoryC);
        return categoryCRepository.save(categoryC);
    }

    @Override
    public List<CategoryC> getAllCategoriesC() {
        return categoryCRepository.findAll();
    }

    @Override
    public Optional<CategoryC> getCategoryCById(Long categoryId) {
        return categoryCRepository.findById(categoryId);
    }

    @Override
    @Transactional
    public CategoryC updateCategoryC(CategoryC categoryC) {
        log.debug("Updated CategoryC: {}", categoryC);
        return categoryCRepository.save(categoryC);
    }

    @Override
    @Transactional
    public boolean deleteCategoryCById(Long categoryId) {
        if (categoryCRepository.existsById(categoryId)) {
            categoryCRepository.deleteById(categoryId);
            log.info("CategoryC with ID {} deleted successfully", categoryId);
            return true;
        }
        log.error("Category with ID {} not found, deletion failed", categoryId);
        return false;
    }

    @Override
    public List<LResponseProduct> getProductsByCategoryC(CategoryC categoryC) {
        List<Product> products = productRepository.findByCategoryC(categoryC);
        List<LResponseProduct> lProducts = new ArrayList<>();
        for (Product product : products) {
            LResponseProduct lResponseProduct = modelMapper.map(product, LResponseProduct.class);
            lResponseProduct.setImageLinks(getFirstImageLink(product));
            lProducts.add(lResponseProduct);
        }
        return lProducts;
    }
    private String getFirstImageLink(Product product) {
        List<ProductImage> productImages = productImageRepository.findByProductId(product.getId());
        if (!productImages.isEmpty()) {
            return productImages.get(0).getLink();
        }
        return null;
    }
}