package com.code.jewelry_shop.security.jwt;

import com.code.jewelry_shop.dto.response.ResponseAccount;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;

import java.io.Serial;
import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
public class JwtAuthResponse implements Serializable {

    @Serial
    private static final long serialVersionUID = 1250166508152483573L;

    private final String token;
    private ResponseAccount account;

    public JwtAuthResponse(String token, ResponseAccount account) {
        this.token = token;
        this.account = account;
    }


}
