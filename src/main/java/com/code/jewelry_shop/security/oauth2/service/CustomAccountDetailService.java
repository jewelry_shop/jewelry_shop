package com.code.jewelry_shop.security.oauth2.service;

import com.code.jewelry_shop.entity.Account;
import com.code.jewelry_shop.repository.AccountRepository;
import com.code.jewelry_shop.security.oauth2.account.AccountPrincipal;
import com.code.jewelry_shop.utils.exception.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomAccountDetailService implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        Account account = accountRepository.findAccountByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Account not Found with username: " + username));
        return AccountPrincipal.build(account);
    }

    @Transactional
    public UserDetails loadUserById(Long id) {
        Account account = accountRepository.findById(id).orElseThrow(
                () -> new ApiException("Account not found with id: {}"+ id)
        );

        return AccountPrincipal.build(account);
    }
}
