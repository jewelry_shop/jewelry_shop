package com.code.jewelry_shop.security.oauth2.service;

import com.code.jewelry_shop.entity.Account;
import com.code.jewelry_shop.entity.User;
import com.code.jewelry_shop.repository.AccountRepository;
import com.code.jewelry_shop.repository.UserRepository;
import com.code.jewelry_shop.security.oauth2.account.AccountPrincipal;
import com.code.jewelry_shop.security.oauth2.account.OAuth2AccountInfo;
import com.code.jewelry_shop.security.oauth2.account.OAuth2AccountInfoFactory;
import com.code.jewelry_shop.utils.constant.AuthProvider;
import com.code.jewelry_shop.utils.exception.OAuth2AuthenticationProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Service
public class CustomOAuth2AccountService extends DefaultOAuth2UserService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public OAuth2User loadUser(OAuth2UserRequest oAuth2UserRequest) throws OAuth2AuthenticationException {
        OAuth2User oAuth2User = super.loadUser(oAuth2UserRequest);

        try {
            return processOAuth2User(oAuth2UserRequest, oAuth2User);
        } catch (AuthenticationException ex) {
            throw ex;
        } catch (Exception ex) {
            // Throwing an instance of AuthenticationException will trigger the OAuth2AuthenticationFailureHandler
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex.getCause());
        }
    }

    private OAuth2User processOAuth2User(OAuth2UserRequest oAuth2UserRequest, OAuth2User oAuth2User) {
        OAuth2AccountInfo oAuth2AccountInfo = OAuth2AccountInfoFactory.getOAuth2AccountInfo(oAuth2UserRequest.getClientRegistration().getRegistrationId(), oAuth2User.getAttributes());
        if(StringUtils.isEmpty(oAuth2AccountInfo.getEmail())) {
            throw new OAuth2AuthenticationProcessingException("Email not found from OAuth2 provider");
        }

        Optional<Account> accountOptional = accountRepository.findAccountByUsername(oAuth2AccountInfo.getEmail());
        Account account;
        if(accountOptional.isPresent()) {
            account = accountOptional.get();
            if(!account.getProvider().equals(AuthProvider.valueOf(oAuth2UserRequest.getClientRegistration().getRegistrationId()))) {
                throw new OAuth2AuthenticationProcessingException("Looks like you're signed up with " +
                        account.getProvider() + " account. Please use your " + account.getProvider() +
                        " account to login.");
            }
            account = updateExistingAccount(account, oAuth2AccountInfo);
        } else {
            account = registerNewAccount(oAuth2UserRequest, oAuth2AccountInfo);
        }

        return AccountPrincipal.create(account, oAuth2User.getAttributes());
    }

    private Account registerNewAccount(OAuth2UserRequest oAuth2UserRequest, OAuth2AccountInfo oAuth2AccountInfo) {
        Account account = new Account();
        User user = new User();

        account.setProvider(AuthProvider.valueOf(oAuth2UserRequest.getClientRegistration().getRegistrationId()));
        account.setProviderId(oAuth2AccountInfo.getId());
        user.setFullName(oAuth2AccountInfo.getName());
        user.setEmail(oAuth2AccountInfo.getEmail());
        account.setUsername(oAuth2AccountInfo.getEmail());
        user.setAvatar(oAuth2AccountInfo.getImageUrl());
        userRepository.save(user);

        return accountRepository.save(account);
    }

    private Account updateExistingAccount(Account existingAccount, OAuth2AccountInfo oAuth2AccountInfo) {
        existingAccount.setUsername(oAuth2AccountInfo.getEmail());
        Optional<User> optionalUser = userRepository.findByAccountId(existingAccount.getId());
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setFullName(oAuth2AccountInfo.getName());
            user.setAvatar(oAuth2AccountInfo.getImageUrl());
            userRepository.save(user);
        }

        return accountRepository.save(existingAccount);
    }

}
