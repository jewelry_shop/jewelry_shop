package com.code.jewelry_shop.security.oauth2.account;

import com.code.jewelry_shop.utils.constant.AuthProvider;
import com.code.jewelry_shop.utils.exception.OAuth2AuthenticationProcessingException;

import java.util.Map;

public class OAuth2AccountInfoFactory {

    public static OAuth2AccountInfo getOAuth2AccountInfo(String registrationId, Map<String, Object> attributes) {
        if(registrationId.equalsIgnoreCase(AuthProvider.google.toString())) {
            return new GoogleOAuth2AccountInfo(attributes);
        } else if (registrationId.equalsIgnoreCase(AuthProvider.facebook.toString())) {
            return new FacebookOAuth2AccountInfo(attributes);
        } else if (registrationId.equalsIgnoreCase(AuthProvider.github.toString())) {
            return new GithubOAuth2AccountInfo(attributes);
        } else {
            throw new OAuth2AuthenticationProcessingException("Sorry! Login with " + registrationId + " is not supported yet.");
        }
    }
}
