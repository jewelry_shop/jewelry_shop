package com.code.jewelry_shop.security.oauth2.account;

import java.util.Map;

public class GithubOAuth2AccountInfo extends OAuth2AccountInfo {

    public GithubOAuth2AccountInfo(Map<String, Object> attributes) {
        super(attributes);
    }

    @Override
    public Long getId() {
        return ((Long) attributes.get("id"));
    }

    @Override
    public String getName() {
        return (String) attributes.get("name");
    }

    @Override
    public String getEmail() {
        return (String) attributes.get("email");
    }

    @Override
    public String getImageUrl() {
        return (String) attributes.get("avatar_url");
    }
}
