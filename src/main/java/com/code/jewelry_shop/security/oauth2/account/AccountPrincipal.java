package com.code.jewelry_shop.security.oauth2.account;

import com.code.jewelry_shop.entity.Account;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class AccountPrincipal implements OAuth2User, UserDetails {

    @Getter
    private Long id;

    private String username;

    private String password;

    private Collection<? extends GrantedAuthority> roles;

    @Setter
    private Map<String, Object> attributes;

    public AccountPrincipal(Long id, String email, String password,
                                Collection<? extends GrantedAuthority> roles) {
        this.id = id;
        this.username = email;
        this.password = password;
        this.roles = roles;
    }

    public static AccountPrincipal build(Account account) {
        List<GrantedAuthority> roles = account.getRole().stream()
                .map(role -> new SimpleGrantedAuthority(role.getRoleName().name()))
                .collect(Collectors.toList());

        return new AccountPrincipal(
                account.getId(),
                account.getUsername(),
                account.getPassword(),
                roles);
    }

    public static AccountPrincipal create(Account account, Map<String, Object> attributes) {
        AccountPrincipal customUserDetails = AccountPrincipal.build(account);
        customUserDetails.setAttributes(attributes);
        return customUserDetails;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }


    public String getEmail() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Map<String, Object> getAttributes() {
        return attributes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AccountPrincipal accountPrincipal = (AccountPrincipal) o;
        return Objects.equals(id, accountPrincipal.id);
    }

    @Override
    public String getName() {
        return String.valueOf(id);
    }
}
