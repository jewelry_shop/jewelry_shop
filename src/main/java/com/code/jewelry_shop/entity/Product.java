package com.code.jewelry_shop.entity;


import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String productName;

    @Column(name = "price", precision = 16, scale = 2)
    private BigDecimal price;

    @Column(name = "discounted_price", precision = 16, scale = 2)
    private BigDecimal discountedPrice;

    private Integer stock;

    @ManyToOne
    @JoinColumn(name = "manufacturer_id")
    private Manufacturer manufacturer;

    @ManyToOne
    @JoinColumn(name = "categoryc_id")
    private CategoryC categoryC;

    @OneToOne
    @JoinColumn(name= "parameter_detail_id")
    private ParameterDetail parameterDetail;

    @ManyToMany
    @JoinTable(
            name = "product_discount",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "discount_id")
    )
    private List<SpecialDiscount> specialDiscounts;

}