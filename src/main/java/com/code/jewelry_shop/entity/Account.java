package com.code.jewelry_shop.entity;

import com.code.jewelry_shop.utils.constant.AuthProvider;
import com.code.jewelry_shop.utils.constant.StatusAccount;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(length = 50)
    @Email
    private String username;

    @NotBlank
    @Column(length = 50)
    private String password;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(length = 10)
    private AuthProvider provider;

    private Long providerId;

    @Column(length = 10)
    @Enumerated(EnumType.STRING)
    private StatusAccount statusAccount;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(  name = "account_role",
            joinColumns = @JoinColumn(name = "account_id"))
    private Set<Role> role = new HashSet<>();
}
