package com.code.jewelry_shop.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SpecialDiscount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 20)
    private String discountName;

    @Column(name = "discount_percentage", precision = 4, scale = 2)
    private BigDecimal discountPercentage;

    private LocalDate startDate;

    private LocalDate endDate;

    @ManyToMany(mappedBy = "discounts")
    private List<Product> products;

}
