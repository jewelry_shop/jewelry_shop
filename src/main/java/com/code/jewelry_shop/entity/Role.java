package com.code.jewelry_shop.entity;

import com.code.jewelry_shop.utils.constant.AuthoritiesConstants;
import jakarta.persistence.*;
import lombok.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 10)
    @Enumerated(EnumType.STRING)
    private AuthoritiesConstants roleName;

    public Role(AuthoritiesConstants roleName) {
        this.roleName = roleName;
    }
}
