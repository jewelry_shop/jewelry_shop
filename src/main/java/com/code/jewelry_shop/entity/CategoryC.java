package com.code.jewelry_shop.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;

@Entity
@Data
public class CategoryC {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50)
    private String categoryName;

    @ManyToOne
    @JoinColumn(name = "category_p_id")
    private CategoryP categoryP;
}
