package com.code.jewelry_shop.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class CategoryP {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_p_id")
    private Long id;

    @Column(length = 50)
    private String categoryPName;
}
