package com.code.jewelry_shop.entity;

import com.code.jewelry_shop.utils.constant.DiscountType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiscountOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(length = 10)
    private DiscountType discountType;

    @Column(precision = 16, scale = 2)
    private BigDecimal conditions;

    @Column(name = "discounted_percentage", precision = 4, scale = 2)
    private BigDecimal discountedPercentage;

    private Integer maxUsage;

    private Integer currentUsage;

    @ManyToOne
    @JoinColumn(name = "discount_code_id")
    private DiscountCode discountCode;

    @ManyToOne
    @JoinColumn(name = "voucher_id")
    private Voucher voucher;

    @JsonIgnore
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Order> orders = new ArrayList<>();
}
