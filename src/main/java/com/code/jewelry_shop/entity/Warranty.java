package com.code.jewelry_shop.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Warranty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 10)
    private String series;

    private String contactInformation;

    @Column(name = "exp_warranty_date")
    private LocalDate expiredWarrantyDate;

    private String status;

    private LocalDateTime requestDate;

    @OneToOne
    @JoinColumn(name = "product_id")
    private Product product;
}
