package com.code.jewelry_shop.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "full_name", length = 100)
    private String fullName;

    @Column(length = 50)
    private String email;

    @Column(length = 20)
    private String phone;

    @Column(name = "dateob")
    private LocalDate dateOfBirth;

    @Size(max = 5)
    private String gender;

    @Column(columnDefinition = "TEXT")
    private String avatar;

    private LocalDateTime createAt;

    private String address;

    @OneToOne
    @JoinColumn(name = "account_id")
    private Account account;

}
