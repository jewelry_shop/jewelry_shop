package com.code.jewelry_shop.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Blog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50)
    private String title;

    @Column(columnDefinition = "TEXT")
    private String content;

    @Column(columnDefinition = "TEXT")
    private String description;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime createAt;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime updateAt;

    @Column(length = 20)
    private String createBy;

    @Column(length = 100)
    private String linkContent;

    @ManyToOne
    @JoinColumn(name = "blog_type_id")
    private BlogType blogType;

}
