package com.code.jewelry_shop.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class Manufacturer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50)
    private String manufacturerName;

}
