package com.code.jewelry_shop;

import com.code.jewelry_shop.config.ApplicationProperties;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springdoc.core.configuration.SpringDocConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@OpenAPIDefinition
@Import({SpringDocConfiguration.class})
@EnableConfigurationProperties(ApplicationProperties.class)
public class JewelryShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(JewelryShopApplication.class, args);
	}

}
