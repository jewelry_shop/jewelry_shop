package com.code.jewelry_shop.dto.response;

import com.code.jewelry_shop.entity.Color;
import lombok.Data;

import java.util.List;

@Data
public class ResponseProductColor {

    private Long productId;

    private List<Color> colors;
}
