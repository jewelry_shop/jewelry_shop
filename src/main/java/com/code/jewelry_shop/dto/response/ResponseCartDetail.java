package com.code.jewelry_shop.dto.response;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ResponseCartDetail {

    private Long id;

    private String productName;

    private String productImageLink;

    private Integer quantity;

    private BigDecimal price;

    private BigDecimal unitPrice;

    private BigDecimal discountedPrice;

    private Long productColor;
}
