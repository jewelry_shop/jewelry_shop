package com.code.jewelry_shop.dto.response.forBlog;

import lombok.Data;

@Data
public class LResponseBlogLink {

    private String linkImage;

    private String linkContent;

    private String link;
}
