package com.code.jewelry_shop.dto.response.forDetail;

import com.code.jewelry_shop.entity.Product;
import com.code.jewelry_shop.entity.ParameterDetail;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DResponseProduct {

    private Product product;

    private Set<String> imageLinks;

    private ParameterDetail parameterDetail;

    private List<Map<String, Object>> productColors;

}
