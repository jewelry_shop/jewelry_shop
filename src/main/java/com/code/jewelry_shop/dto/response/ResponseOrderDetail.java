package com.code.jewelry_shop.dto.response;

import com.code.jewelry_shop.entity.OrderDetail;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseOrderDetail {

    private Long id;

    private Integer quantity;

    private BigDecimal total;

    private BigDecimal unitPrice;

    public ResponseOrderDetail(OrderDetail orderDetail) {
        this.id = orderDetail.getId();
        this.quantity = orderDetail.getQuantity();
        this.total = orderDetail.getSubTotalPrice();
        this.unitPrice = orderDetail.getUnitPrice();
    }

}
