package com.code.jewelry_shop.dto.response;

import com.code.jewelry_shop.utils.constant.AuthProvider;
import com.code.jewelry_shop.utils.constant.StatusAccount;
import lombok.Data;

@Data
public class ResponseAccount {

    private Long id;

    private String username;

    private StatusAccount statusAccount;

    private AuthProvider authProvider;

    private String role;
}
