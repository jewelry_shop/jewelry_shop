package com.code.jewelry_shop.dto.response;

import com.code.jewelry_shop.utils.constant.StatusOrder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class ResponseOrder {

    private Long id;

    private StatusOrder status;

    private BigDecimal totalPrice;

    private LocalDateTime orderDate;

    private List<ResponseOrderDetail> responseOrderDetails;
}
