package com.code.jewelry_shop.dto.response.forDetail;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class DResponseUser {

    private Long id;

    private String fullName;

    private String email;

    private String phone;

    private LocalDate dateOfBirth;

    private String gender;

    private String avatar;

    private LocalDateTime createAt;

    private String address;

    private Long accountId;

}
