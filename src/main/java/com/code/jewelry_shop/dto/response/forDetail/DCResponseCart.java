package com.code.jewelry_shop.dto.response.forDetail;

import com.code.jewelry_shop.dto.response.ResponseOrderDetail;
import com.code.jewelry_shop.entity.OrderDetail;
import com.code.jewelry_shop.utils.constant.StatusOrder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class DCResponseCart {

    private List<ResponseOrderDetail> orderDetail;

    private String fullName;

    private String email;

    private String phone;

    private LocalDate dateOfBirth;

    private String address;

    private StatusOrder statusOrder;
}
