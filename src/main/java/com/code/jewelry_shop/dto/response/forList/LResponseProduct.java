package com.code.jewelry_shop.dto.response.forList;

import lombok.Data;

import java.lang.reflect.Parameter;
import java.math.BigDecimal;

@Data
public class LResponseProduct {

    private Long id;

    private String productName;

    private BigDecimal price;

    private BigDecimal discountedPrice;

    private String imageLinks;
}