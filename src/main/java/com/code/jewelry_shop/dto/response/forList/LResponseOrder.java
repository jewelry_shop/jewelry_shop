package com.code.jewelry_shop.dto.response.forList;

import com.code.jewelry_shop.utils.constant.StatusOrder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class LResponseOrder {

    private Long id;

    private BigDecimal totalPrice;

    private StatusOrder status;

    private LocalDateTime orderDate;

}
