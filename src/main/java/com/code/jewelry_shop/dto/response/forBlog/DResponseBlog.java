package com.code.jewelry_shop.dto.response.forBlog;

import com.code.jewelry_shop.entity.Blog;
import lombok.Data;

import java.util.Set;

@Data
public class DResponseBlog {

    private Blog blog;

    private Set<String> bannerImage;

}
