package com.code.jewelry_shop.dto.response;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ResponseDiscountOrder {

    private Long id;

    private BigDecimal conditions;

    private BigDecimal discountedPercentage;

    private Long orderId;
}
