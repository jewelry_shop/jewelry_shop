package com.code.jewelry_shop.dto.response.forBlog;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

@Data
public class LResponseBlog {

    private String bannerImg;

    private String title;

    private String content;

    private LocalDateTime updateAt;

    private LocalDateTime createBy;

    private String linkContent;

    private String keyword;
}
