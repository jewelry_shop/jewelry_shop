package com.code.jewelry_shop.dto.response.forDetail;

import com.code.jewelry_shop.entity.User;
import lombok.Data;

@Data
public class DResponseAccount {

    private Long id;

    private String username;

    private String status;

    private String role;

    private User user;
}
