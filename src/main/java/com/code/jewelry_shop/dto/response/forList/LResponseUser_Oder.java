package com.code.jewelry_shop.dto.response.forList;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class LResponseUser_Oder {

    private Long id;

    private String fullName;

    private String email;

    private String phone;

    private LocalDate dateOfBirth;

    private String address;

    private List<LResponseOrder> lResponseOrder;

}
