package com.code.jewelry_shop.dto.response.authen;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmailResult {

    private boolean isSuccess;

    private String message;
}
