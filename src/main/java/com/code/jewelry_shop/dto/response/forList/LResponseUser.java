package com.code.jewelry_shop.dto.response.forList;

import lombok.Data;

import java.time.LocalDate;

@Data
public class LResponseUser {

    private Long id;

    private String fullName;

    private String email;

    private String phone;

    private LocalDate dateOfBirth;

    private String gender;

    private String address;
}
