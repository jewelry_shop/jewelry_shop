package com.code.jewelry_shop.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class ResponseCart {

    private Long userId;

    private List<ResponseCartDetail> cartDetailDTOList;
}
