package com.code.jewelry_shop.dto.request.forUpdate;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class URequestDiscountOrder {

    private BigDecimal conditions;

    private BigDecimal discountedPercentage;

    private Integer maxUsage;

    private URequestVoucher uRequestVoucher;

    private URequestDiscountCode uRequestDiscountCode;
}
