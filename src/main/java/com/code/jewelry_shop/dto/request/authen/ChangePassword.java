package com.code.jewelry_shop.dto.request.authen;

import lombok.Data;

@Data
public class ChangePassword {

    private String username;

    private String currentPassword;

    private String newPassword;

    private String verificationCode;
}
