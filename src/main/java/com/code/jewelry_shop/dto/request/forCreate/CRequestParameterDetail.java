package com.code.jewelry_shop.dto.request.forCreate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CRequestParameterDetail {

    private Integer size;

    private String ownerSize;

    private String style;

    private String ownerStyle;

    private Integer niHand;

    private String material;

    private String gemstone;

    private Long productId;
}
