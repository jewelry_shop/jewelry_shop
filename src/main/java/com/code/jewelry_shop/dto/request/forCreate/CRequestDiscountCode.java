package com.code.jewelry_shop.dto.request.forCreate;

import lombok.Data;

import java.time.LocalDate;

@Data
public class CRequestDiscountCode {

    private String discountCodeName;

    private LocalDate expiredDate;

}
