package com.code.jewelry_shop.dto.request.forCreate;

import com.code.jewelry_shop.entity.DiscountCode;
import com.code.jewelry_shop.entity.Voucher;
import com.code.jewelry_shop.utils.constant.DiscountType;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CRequestDiscountOrder {

    private DiscountType discountType;

    private BigDecimal conditions;

    private BigDecimal discountedPercentage;

    private Integer maxUsage;

    private Integer currentUsage;

    private CRequestVoucher voucher;

    private CRequestDiscountCode discountCode;
}
