package com.code.jewelry_shop.dto.request.forCreate;

import lombok.Data;

import java.time.LocalDate;

@Data
public class CRequestVoucher {

    private String voucherName;

    private String voucherCode;

    private LocalDate validFrom;

    private LocalDate validTo;
}
