package com.code.jewelry_shop.dto.request.forUpdate;

import lombok.Data;

@Data
public class URequestParameterDetail {

    private Long id;

    private String parameterDetailName;

    private Integer ownerSize;

    private String style;

    private String ownerStyle;

    private Integer niHand;

}
