package com.code.jewelry_shop.dto.request.forUpdate;

import com.code.jewelry_shop.entity.*;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

@Data
public class URequestProduct {

    private Long id;

    private String productName;

    private BigDecimal price;

    private Integer stock;

    private List<SpecialDiscount> specialDiscounts;

    private CategoryC categoryC;

    private Manufacturer manufacturer;

    private ParameterDetail parameterDetail;

    private MultipartFile[] files;

    private Set<String> imageLinks;
}
