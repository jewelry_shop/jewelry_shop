package com.code.jewelry_shop.dto.request.forCreate;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

@Data
public class URequestBlog {

    private Long id;

    private String title;

    private String content;

    private String description;

    private LocalDateTime updateAt;

    private String createBy;

    private String linkContent;

    private MultipartFile[] files;

    private Long blogTypeId;
}
