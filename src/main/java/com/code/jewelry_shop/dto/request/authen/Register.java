package com.code.jewelry_shop.dto.request.authen;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class Register {

    private String fullName;

    private String userName;

    private String password;

    private Long roleId;
}