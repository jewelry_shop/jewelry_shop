package com.code.jewelry_shop.dto.request.forCreate;

import com.code.jewelry_shop.entity.*;
import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.List;

@Data
public class CRequestProduct {

    private String productName;

    private BigDecimal price;

    private Integer stock;

    private List<SpecialDiscount> specialDiscounts;

    private Manufacturer manufacturer;

    private CategoryC categoryC;

    private ParameterDetail parameterDetail;

    private MultipartFile[] files;

    private List<Long> colors;
}
