package com.code.jewelry_shop.dto.request.forUpdate;

import lombok.Data;

import java.time.LocalDate;

@Data
public class URequestVoucher {

    private Long id;

    private String voucherName;

    private String voucherCode;

    private LocalDate validFrom;

    private LocalDate validTo;
}
