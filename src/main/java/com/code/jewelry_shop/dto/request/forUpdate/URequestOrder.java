package com.code.jewelry_shop.dto.request.forUpdate;

import com.code.jewelry_shop.utils.constant.StatusOrder;
import lombok.Data;

@Data
public class URequestOrder {

    private Long id;

    private String fullName;

    private String phoneNumber;

    private StatusOrder status;
}
