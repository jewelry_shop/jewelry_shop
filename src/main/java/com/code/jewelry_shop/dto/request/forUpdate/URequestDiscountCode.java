package com.code.jewelry_shop.dto.request.forUpdate;

import lombok.Data;

import java.time.LocalDate;

@Data
public class URequestDiscountCode {

    private Long id;

    private String discountCodeName;

    private LocalDate expiredDate;
}
