package com.code.jewelry_shop.dto.request.forCreate;

import lombok.Data;

import java.time.LocalDate;

@Data
public class CRequestWarranty {

    private String contactInformation;

    private LocalDate expiredWarrantyDate;
}
