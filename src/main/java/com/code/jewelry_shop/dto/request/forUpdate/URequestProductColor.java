package com.code.jewelry_shop.dto.request.forUpdate;

import lombok.Data;

import java.util.List;

@Data
public class URequestProductColor {

    private Long productId;

    private List<Long> colors;
}
