package com.code.jewelry_shop.dto.request.forCreate;

import com.code.jewelry_shop.entity.BlogType;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

@Data
public class CRequestBlog {

    private String title;

    private String content;

    private String description;

    private LocalDateTime createAt;

    private LocalDateTime updateAt;

    private String createBy;

    private String linkContent;

    private MultipartFile[] files;

    private Long blogTypeId;
}
