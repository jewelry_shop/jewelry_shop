package com.code.jewelry_shop.dto.request.authen;

import lombok.Data;

@Data
public class LoginModel {

    private String username;

    private String password;
}
