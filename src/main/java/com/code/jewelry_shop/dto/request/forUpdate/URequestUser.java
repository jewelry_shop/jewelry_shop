package com.code.jewelry_shop.dto.request.forUpdate;

import lombok.Data;

import java.time.LocalDate;

@Data
public class URequestUser {

    private Long id;

    private String fullName;

    private String phone;

    private String gender;

    private LocalDate dateOfBirth;

    private String address;
}
