package com.code.jewelry_shop.dto.request.forCreate;

import lombok.Data;

@Data
public class CRequestSpecialDiscount {

    private String discountName;

    private Double discountPercentage;

    private String startDate;

    private String endDate;

}
