package com.code.jewelry_shop.dto.request.forCreate;

import com.code.jewelry_shop.utils.constant.StatusOrder;
import lombok.Data;

import java.time.LocalDate;

@Data
public class CRequestOrder {

    private String fullName;

    private String email;

    private String phone;

    private LocalDate dateOfBirth;

    private String address;

}
