package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.entity.BlogImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BlogImageRepository extends JpaRepository<BlogImage, Long> {
    @Query("DELETE FROM BlogImage pi WHERE pi.blog.id = :blogId")
    void deleteByBlogId(Long id);
    List<BlogImage> findByBlogId(Long blogId);
}
