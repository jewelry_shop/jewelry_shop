package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.entity.CategoryP;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryPRepository extends JpaRepository<CategoryP, Long> {
    CategoryP findByCategoryPName(String name);
}
