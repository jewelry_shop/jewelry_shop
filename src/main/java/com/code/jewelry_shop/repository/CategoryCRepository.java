package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.entity.CategoryC;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryCRepository extends JpaRepository<CategoryC, Long> {
}