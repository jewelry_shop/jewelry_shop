package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.entity.Role;
import com.code.jewelry_shop.utils.constant.AuthoritiesConstants;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByRoleName(AuthoritiesConstants name);
}