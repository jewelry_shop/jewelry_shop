package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.entity.CategoryC;
import com.code.jewelry_shop.entity.Manufacturer;
import com.code.jewelry_shop.entity.Product;
import com.code.jewelry_shop.entity.SpecialDiscount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findByCategoryC(CategoryC categoryC);

    List<Product> findByManufacturer(Manufacturer manufacturer);

    List<Product> findByProductNameContainingIgnoreCase(String keyword);

    Page<Product> findAll(Pageable pageable);
}

