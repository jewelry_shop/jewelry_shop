package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.entity.Warranty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WarrantyRepository extends JpaRepository<Warranty, Long> {

    Warranty findBySeries(String serialNumber);
}