package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.entity.Product;
import com.code.jewelry_shop.entity.ProductColor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductColorRepository extends JpaRepository<ProductColor, Long> {

    boolean existsByProductAndColorId(Product product, Long colorId);

    List<ProductColor> findAllByProductId(Long productId);

    @Query("DELETE FROM ProductColor pi WHERE pi.product.id = :productId")
    void deleteByProductId(@Param("productId") Long productId);

    ProductColor findByProductIdAndColorId(Long productId, Long colorId);
}

