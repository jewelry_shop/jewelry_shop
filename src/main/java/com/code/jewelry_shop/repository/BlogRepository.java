package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.entity.Blog;
import com.code.jewelry_shop.entity.BlogType;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BlogRepository extends JpaRepository<Blog, Long> {
    List<Blog> findAllByBlogType(BlogType blogType);
    Optional<Blog> findFirstByLinkContentAndBlogType(String linkContent, BlogType blogType);
    List<Blog> findByTitleContainingIgnoreCase(String title);
    Blog findFirstByLinkContent(String linkContent);
    @Modifying
    @Query("DELETE FROM Blog b WHERE b.id = :blogId")
    void deleteBlogById(Long blogId);
    Integer countAllByBlogType(BlogType blogType);
}
