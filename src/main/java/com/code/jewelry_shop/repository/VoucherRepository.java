package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.entity.Voucher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VoucherRepository extends JpaRepository<Voucher, Long> {

    Optional<Voucher> findByVoucherCode(String code);

    boolean existsByVoucherNameOrVoucherCode(String voucherName, String voucherCode);
}