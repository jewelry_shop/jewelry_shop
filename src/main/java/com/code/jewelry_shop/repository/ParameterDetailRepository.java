package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.entity.ParameterDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface ParameterDetailRepository extends JpaRepository<ParameterDetail, Long> {
    ParameterDetail findByProductId(Long productId);

//    boolean existsByProductAndParameterName(Product product, String parameterName);


    @Modifying
    @Query("DELETE FROM ParameterDetail ds WHERE ds.product.id = :productId")
    void deleteByProductId(@Param("productId") Long productId);
}
