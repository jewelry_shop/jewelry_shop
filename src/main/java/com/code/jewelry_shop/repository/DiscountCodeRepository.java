package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.entity.DiscountCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DiscountCodeRepository extends JpaRepository<DiscountCode, Long> {

    Optional<DiscountCode> findByDiscountCode(String code);

    boolean existsByDiscountCodeName(String discountName);
}
