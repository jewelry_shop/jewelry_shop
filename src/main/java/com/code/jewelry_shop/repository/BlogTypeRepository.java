package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.entity.BlogType;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

@Repository
public interface BlogTypeRepository extends JpaRepository<BlogType, Long> {
    BlogType findFirstByLink(String link);
}
