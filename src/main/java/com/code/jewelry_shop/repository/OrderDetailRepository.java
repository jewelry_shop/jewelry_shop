package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.entity.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long> {
    List<OrderDetail> findAllByOrderId_Id(Long orderId);

    OrderDetail findByOrder_Id(Long orderId);
}