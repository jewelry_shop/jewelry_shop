package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.dto.response.ResponseAccount;
import com.code.jewelry_shop.entity.Account;
import com.code.jewelry_shop.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findByUsername(String username);

    @Query("SELECT a FROM Account a WHERE a.username = ?1")
    Optional<Account> findAccountByUsername(String username);

    List<Account> findAllByRole(Set<Role> role);


}
