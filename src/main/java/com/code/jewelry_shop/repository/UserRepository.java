package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.dto.response.forList.LResponseUser;
import com.code.jewelry_shop.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByAccountId(Long accountId);

    void deleteByAccountId(Long id);

//    @Query("SELECT u FROM User u WHERE u.phone = :phone")
//    User findUserByPhone(@Param("phone") String phone);
    User findByPhone(String phone);
}