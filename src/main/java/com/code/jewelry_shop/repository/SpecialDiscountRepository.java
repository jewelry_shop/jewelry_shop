package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.entity.SpecialDiscount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpecialDiscountRepository extends JpaRepository<SpecialDiscount, Long> {
    List<SpecialDiscount> findAllByDiscountNameContains(String discount);
}
