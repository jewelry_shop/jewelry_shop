package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.entity.Cart;
import com.code.jewelry_shop.entity.CartDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CartDetailRepository extends JpaRepository<CartDetail, Long> {

    List<CartDetail> findByCart(Cart id);
}
