package com.code.jewelry_shop.repository;

import com.code.jewelry_shop.entity.DiscountOrder;
import com.code.jewelry_shop.entity.Order;
import com.code.jewelry_shop.utils.constant.DiscountType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DiscountOrderRepository extends JpaRepository<DiscountOrder, Long> {

    List<DiscountOrder> findByOrdersContaining(Order order);

    List<DiscountOrder> findByDiscountType(DiscountType discountType);

    DiscountOrder findByVoucher_Id(Long id);

    DiscountOrder findByDiscountCode_Id(Long id);
}
