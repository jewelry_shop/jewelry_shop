package com.code.jewelry_shop.config;

import com.code.jewelry_shop.service.impl.FirebaseImageService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FirebaseImageServiceConfig {

    @Bean
    @Qualifier("firebaseImageServiceProperties")
    @ConfigurationProperties(prefix = "firebase")
    public FirebaseImageService.Properties firebaseImageServiceProperties() {
        return new FirebaseImageService.Properties();
    }

}
