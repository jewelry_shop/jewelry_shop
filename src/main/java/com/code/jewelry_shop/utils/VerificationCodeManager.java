package com.code.jewelry_shop.utils;

import java.util.HashMap;
import java.util.Map;

public class VerificationCodeManager {
    private static final Map<String, String> verificationCodes = new HashMap<>();
    public static void saveVerificationCode(String username, String verificationCode){
        verificationCodes.put(username, verificationCode);
    }
    public static void removeVerificationCode(String username){
        verificationCodes.remove(username);
    }
    public static boolean isVerificationCodeValid(String username, String verificationCode){
        String savedVerificationCode = verificationCodes.get(username);
        return savedVerificationCode != null && savedVerificationCode.equals(verificationCode);
    }
}
