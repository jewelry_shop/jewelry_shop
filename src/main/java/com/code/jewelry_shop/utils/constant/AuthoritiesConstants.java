package com.code.jewelry_shop.utils.constant;

public enum AuthoritiesConstants {
    ROLE_CUSTOMER,
    ROLE_MANAGER,
    ROLE_ADMIN
}
