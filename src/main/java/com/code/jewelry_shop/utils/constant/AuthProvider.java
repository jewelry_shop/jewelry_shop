package com.code.jewelry_shop.utils.constant;

public enum AuthProvider {
    local,
    google,
    facebook,
    github
}
