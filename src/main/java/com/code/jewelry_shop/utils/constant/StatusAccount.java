package com.code.jewelry_shop.utils.constant;

public enum StatusAccount {

    ENABLED,

    LOCKED
}
