package com.code.jewelry_shop.utils.constant;

public enum StatusOrder {
    Pending_Confirmation,
    Confirmed,
    In_Transit,
    Delivered,
    Cancelled

}
