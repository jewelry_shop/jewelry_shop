package com.code.jewelry_shop.utils.constant;

public enum DiscountType {
    DISCOUNT_CODE,
    VOUCHER
}